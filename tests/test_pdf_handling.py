from pathlib import Path
from exam_scan_tools import pdf
from io import BytesIO


def test_pdf_single_page():
    pdf_path = Path(__file__).parent / "../exam_scan_tools/assets/Sleeping-Kitty.pdf"
    l_pdf = pdf.LazyPdf(pdf_path, 0)

    with open(pdf_path, mode="rb") as f_handle:
        pdfstream = BytesIO(f_handle.read())

    pdfstream.seek(0)
    m_pdf = pdf.MemoryPdf(pdfstream)

    assert (
        abs(m_pdf.get_page().get_size().width - l_pdf.get_page().get_size().width)
        < 0.001
    )
    assert (
        abs(m_pdf.get_page().get_size().height - l_pdf.get_page().get_size().height)
        < 0.001
    )
    assert m_pdf.get_page().get_text() == l_pdf.get_page().get_text()

    mreader_page = m_pdf.get_reader_page()
    lreader_page = l_pdf.get_reader_page()

    assert mreader_page.get_contents()._data == lreader_page.getContents()._data


def test_pdf_lazy():
    pdf_path = Path(__file__).parent / "../resources/sample_input/sample_exam.pdf"
    lpdf_1 = pdf.LazyPdf(pdf_path, 1)
    lpdf_2 = pdf.LazyPdf(pdf_path, 2)

    assert lpdf_1.get_page().get_text() != lpdf_2.get_page().get_text()
    assert (
        lpdf_1.get_page().get_text()
        == "Surname:\nMatriculation Nr.:\nThis page does not contain any tasks\nPage ii\nACS Example Subject Exam - 13.09.2042"
    )

    lreader_page_1 = lpdf_1.get_reader_page()
    lreader_page_2 = lpdf_2.get_reader_page()

    assert lreader_page_1.get_contents()._data != lreader_page_2.getContents()._data


def test_pdf_image():
    pdf_path = Path(__file__).parent / "../resources/sample_input/sample_exam.pdf"
    lpdf_1 = pdf.LazyPdf(pdf_path, 1)
    img1 = lpdf_1.get_page_as_image(resolution=300)

    lpdf_2 = pdf.LazyPdf(pdf_path, 2)
    img2 = lpdf_2.get_page_as_image(resolution=300)

    assert img1.signature != img2.signature


def test_pdf_image2():
    pdf_path = Path(__file__).parent / "../exam_scan_tools/assets/Sleeping-Kitty.pdf"
    l_pdf = pdf.LazyPdf(pdf_path, 0)

    with open(pdf_path, mode="rb") as f_handle:
        pdfstream = BytesIO(f_handle.read())

    pdfstream.seek(0)
    m_pdf = pdf.MemoryPdf(pdfstream)

    l_img = l_pdf.get_page_as_image(resolution=300)
    m_img = m_pdf.get_page_as_image(resolution=300)

    assert l_img.signature == m_img.signature


def test_rotation():
    pdf_path = Path(__file__).parent / "../exam_scan_tools/assets/Sleeping-Kitty.pdf"
    pdf_normal = pdf.LazyPdf(pdf_path, 0)
    pdf_flipped = pdf.LazyPdf(pdf_path, 0, rotation=pdf.Rotation.FLIPPED)

    assert pdf_normal.get_reader_page().__getitem__("/Rotate") == 0
    assert pdf_flipped.get_reader_page().__getitem__("/Rotate") == 180

    assert (
        abs(
            pdf_normal.get_page().get_size().width
            - pdf_flipped.get_page().get_size().width
        )
        < 0.001
    )
    assert (
        abs(
            pdf_normal.get_page().get_size().height
            - pdf_flipped.get_page().get_size().height
        )
        < 0.001
    )

    n_img = pdf_normal.get_page_as_image(resolution=300)
    f_img = pdf_flipped.get_page_as_image(resolution=300)

    assert n_img.signature != f_img.signature
    # A number of parameters which shouldn't change on rotation
    assert n_img.red_primary == f_img.red_primary
    assert n_img.width == f_img.width
    assert (n_img.skewness - f_img.skewness) < 0.001
    assert (n_img.kurtosis - f_img.kurtosis) < 0.001
