# Exam Scan Manager

This is e tool which helps you to digitalize exams by generation and recognition of unique Barcodes on every page of an exam.
This can be useful to:
- Do online reviews of hand-written exams
- Digitally store the Exams

## Workflow

The following graph illustrates the workflow for such an exam:
This tool performs the the steps $\textcolor{red}{\text{colored Red}}$!

![Flowchart](resources/FlowChart.svg)

## Setup

### Python on Linux

Optional: Use a _python venv_:

```bash
sudo apt install python3-venv  # if not installed already
python -m venv .env
source .env/bin/activate
```

#### Install all dependencies:

- Ubuntu:

```bash
sudo apt install python3-pip libgirepository1.0-dev gir1.2-gtk-3.0 libpoppler-dev libpoppler-cpp-dev cmake gir1.2-poppler-0.18 python3-pythonmagick python3-zbar
```

- Fedora:

```bash
sudo dnf install poppler-cpp-devel cmake cairo-gobject-devel
```

#### Installation

Regular:
```bash
# in the projects root folder:
pip install .
```

Development installation:
```bash
# in the projects root folder:
pip install -e .
```

#### Execution

Run it:
```bash
exam_scan_manager
```

### Windows

The following programs need to be installed:

- Imagemagick: https://imagemagick.org/script/download.php#windows
- Ghostscript: https://www.ghostscript.com/download/gsdnld.html

You can then download the exe file [here](https://git.rwth-aachen.de/acs/public/exam-tools/exam_scan_manager/-/jobs/artifacts/ci/download?job=win-build).
Extract the archive and double-click the `exam-scan-manager.exe`.

#### Development build on Windows

You need to install [MSYS2](https://www.msys2.org/).
Please execute the `pacman` and `pip` installation instructions from the [CI File](.gitlab-ci.yml) in the _mingw64_ shell.
You should then be able to execute the python files from that shell as well.

## Troubleshooting:

### Policy Error:

If you get the following Error:
```
wand.exceptions.PolicyError: attempt to perform an operation not allowed by the security policy `PDF' @ error/constitute.c/IsCoderAuthorized/408
```

Then you have to modify the imagemagick security policies.

**Check that the installed ghostscript version is larger 9.24! Otherwise this is a security risk**
```bash
gs --version # must be greater than 9.24
```

In `/etc/ImageMagick-*/policy.xml` Change this line:
```xml
  <policy domain="coder" rights="none" pattern="PDF" />
```
to:
```xml
  <policy domain="coder" rights="read | write" pattern="PDF" />
```

Background information can be found [here](https://www.kb.cert.org/vuls/id/332928/)

## License

This tool is licensed under the [GPLv3](LICENSE).
Libraries contained in the Windows package are unmodified and are licensed under their respective license.


## Gallery

The first step is to generate exams with customized Barcodes containing a random ID:

![Barcode Creation](resources/screenshots/Barcode_Creation.png)
After the exam, all exams have to be scanned - e.g. as a batch by a office-scanner. These scans can now be sorted by the unique Barcodes.

![Barcode Scanning](resources/screenshots/scanning.png)

With this tool the scanned exams can now be checked for completeness and the mapping from the random IDs to the student id can be entered.

![Scan Manager](resources/screenshots/scan_manager.png)

To speed up this tedious process, a special mode shows you the first page of each exam, so that you can rapidly fill the student ids:

![Interactive Input](resources/screenshots/interactive_input.png)
