from enum import Enum
from logging import warning
from typing import Dict, Iterable, Iterator, List, Optional, Tuple

from .pdf import PdfProvider


class ScannedPage:
    exam_id: Optional[str]
    page_nr: Optional[int]
    pdf: PdfProvider

    def __init__(
        self, exam_id: Optional[str], page_nr: Optional[int], pdf: PdfProvider
    ):
        self.exam_id = exam_id
        self.page_nr = page_nr
        self.pdf = pdf

    @classmethod
    def from_exam_page(cls, exam_id: Optional[str], exam_page: "ExamPage"):
        return cls(exam_id, exam_page.page_nr, exam_page.pdf)

    def __eq__(self, other):
        if not isinstance(other, ExamPage):
            return NotImplemented
        return self.pdf == other.pdf


class ExamPage:
    page_nr: int
    pdf: PdfProvider

    def __init__(self, page_nr: int, pdf: PdfProvider):
        self.page_nr = page_nr
        self.pdf = pdf

    @classmethod
    def from_scanned_page(cls, scanned_page: ScannedPage):
        assert isinstance(scanned_page.page_nr, int)
        return cls(scanned_page.page_nr, scanned_page.pdf)

    def __eq__(self, other):
        if not isinstance(other, ExamPage):
            return NotImplemented
        return self.page_nr == other.page_nr and self.pdf == other.pdf


class Exam:
    exam_id: str
    student_id: Optional[int]
    pages: List[ExamPage]

    def __init__(self, exam_id: str, pages: Optional[List[ExamPage]]):
        self.exam_id = exam_id
        self.student_id = None
        if pages is not None:
            self.pages = pages
        else:
            self.pages = []

    def append(self, page: ExamPage):
        self.pages.append(page)

    def first_page(self) -> ExamPage:
        return sorted(self.pages, key=lambda page: int(page.page_nr))[0]

    def contains(self, page: ExamPage) -> bool:
        return page in self.pages


class ExamTableState(Enum):
    VALID = 1
    EMPTY = 2
    WRONG_PAGECNT = 3
    MISSING_ID = 4


class ExamTable:
    _exams: Dict[str, Exam]

    def __init__(self):
        self._exams = {}

    def exams(self) -> Iterator[Exam]:
        for exam_id, exam in self._exams.items():
            yield exam

    def insert_page(self, page: ScannedPage):
        assert isinstance(page.exam_id, str)
        if page.exam_id not in self._exams:
            new_exam = Exam(page.exam_id, [ExamPage.from_scanned_page(page)])
            self.insert_exam(new_exam)
        else:
            self._exams[page.exam_id].append(ExamPage.from_scanned_page(page))

    def insert_pages(self, pages: Iterable[ScannedPage]):
        for page in pages:
            self.insert_page(page)

    def remove_page(self, exam_id: str, page: ExamPage):
        for p in self._exams[exam_id].pages:
            if p.page_nr == page.page_nr and p.pdf == page.pdf:
                self._exams[exam_id].pages.remove(p)
                break

        if len(self._exams[exam_id].pages) == 0:
            del self._exams[exam_id]

    def insert_exam(self, new_exam: Exam):
        assert new_exam.exam_id not in self._exams
        self._exams[new_exam.exam_id] = new_exam

    def update_student_id(self, exam_id: str, student_id: int):
        self._exams[exam_id].student_id = student_id

    def is_empty(self) -> bool:
        return len(self._exams) == 0

    def check_validity(
        self, pagecount: Optional[int]
    ) -> Tuple[ExamTableState, Optional[str]]:
        """
        Returns the ExamTableState enum, and - if necessary - the corresponding exam_id.
        """
        if self.is_empty():
            warning("Examtable is empty")
            return (ExamTableState.EMPTY, None)

        for exam_id, exam in self._exams.items():
            if exam.student_id is None:
                warning(f"Exam {exam_id} was not yet assigned a valid student id")
                return (ExamTableState.MISSING_ID, exam_id)
            if pagecount is not None and len(exam.pages) != pagecount:
                warning(
                    f"Exam {exam_id} has invalid page count: {len(exam.pages)}/{pagecount}"
                )
                return (ExamTableState.WRONG_PAGECNT, exam_id)

        return (ExamTableState.VALID, None)
