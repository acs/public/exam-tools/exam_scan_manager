from typing import Optional

from gi.repository import Gtk


class ExamIdEntry(Gtk.Entry, Gtk.Editable):
    def __init__(
        self,
        completion_model: Optional[Gtk.ListStore] = None,
    ):
        super(ExamIdEntry, self).__init__()
        self.set_alignment(0.5)
        self.set_placeholder_text("Exam ID")
        self.set_width_chars(8)

        if completion_model is not None:
            completion = Gtk.EntryCompletion()
            completion.set_model(completion_model)
            completion.set_text_column(0)
            self.set_completion(completion)

    def do_insert_text(self, new_text, length, position):
        self.get_buffer().insert_text(position, new_text.upper(), length)
        return position + length
