from __future__ import annotations

import csv
from enum import IntEnum
from logging import info
from pathlib import Path
from typing import TYPE_CHECKING, Generic, Iterable, List, Optional, TypeVar

from gi.repository import Gdk, Gtk

from .exam import Exam, ExamPage, ExamTable, ExamTableState, ScannedPage
from .exam_parameters_box import ExamParametersBox, NewData
from .export import ExportDialog, warn_export_dialog
from .gui_helpers import get_content
from .id_mapping import CsvMappingDialog
from .manual_matcher import ManualMatchingDialog
from .new_scan import NewScan
from .pdf import LazyPdf
from .pdf_view import PdfView
from .student_id_input import InteractiveStudentIdInput
from .student_id_list import StudentIdListDialog

if TYPE_CHECKING:
    from .main_window import MainWindow


@Gtk.Template(filename=str((Path(__file__) / "../glade/Scan_Sorter.glade").resolve()))
class ScanSorter(Gtk.Box):
    __gtype_name__ = "scan_sorter"

    autocompl_button = Gtk.Template.Child("autocompl_button")
    autocompl_popover = Gtk.Template.Child("autocompl_popover")
    del_autocompl_button = Gtk.Template.Child("del_autocompl_button")
    deleted_pages_area = Gtk.Template.Child("deleted_pages_area")
    deleted_pages_vp = Gtk.Template.Child("deleted_pages_vp")
    exam_count_label = Gtk.Template.Child("exam_count_label")
    mapping_hbox = Gtk.Template.Child("mapping_hbox")
    matching_list = Gtk.Template.Child("exam_list")
    nr_pages_entry = Gtk.Template.Child("nr_pages_entry")
    header_row = Gtk.Template.Child("header_row")

    def __init__(self, parent: MainWindow):

        super(Gtk.Box, self).__init__()
        self.parent = parent
        self.parent.window = parent.window

        self.examtable: ExamTable = ExamTable()
        self.exam_rows: List["ExamListRow"] = []
        self.student_id_suggestions: Gtk.ListStore = Gtk.ListStore(str)
        self.current_exam_ids: Gtk.ListStore = Gtk.ListStore(str)

        # PDF preview area:
        self.pdf_view = PdfView(
            LazyPdf((Path(__file__) / "../assets/scan_sorter_background.pdf"), 0)
        )
        self.mapping_hbox.pack_start(self.pdf_view, True, True, 5)

        self.exam_parameter_box = ExamParametersBox(self.current_exam_ids)
        self.mapping_hbox.pack_end(self.exam_parameter_box, False, True, 5)

        self.deleted_pages: List[ScannedPage] = []

        # Default values required to clear the rows in the examtable
        self.del_pages_area = DeletedPagesArea(
            self,
            self.deleted_pages_area,
            self.deleted_pages_vp,
        )

        self.nr_pages_entry.connect("changed", self.update_page_nr_highlight)

        self.del_autocompl_button.set_sensitive(False)

    def new_scan(self, widget):
        confirmation = None
        if not self.examtable.is_empty():
            confirmation = confirm_new_scan(self.parent.window)
            if (
                confirmation != NewScanAction.APPEND
                and confirmation != NewScanAction.NEW_TABLE
            ):
                return

        new_scan_dialog = NewScan(self.parent.window)
        result = new_scan_dialog.run()
        if result is None:
            return

        (failed_scans, successful_scans) = result

        widget.get_style_context().remove_class("suggested-action")

        uniqe_exam_ids = set()
        for scan in successful_scans:
            uniqe_exam_ids.add(scan.exam_id)

        tmp_new_exam_ids = Gtk.ListStore(str)
        append_list_store(tmp_new_exam_ids, uniqe_exam_ids)

        identified_failures = None
        if len(failed_scans) > 0:
            matcher_dialog = ManualMatchingDialog(
                self.parent.window, failed_scans, tmp_new_exam_ids
            )
            result = matcher_dialog.run()
            if result is None:
                # self.examtable = None
                return
            (identified_failures, self.deleted_pages) = result

        if confirmation == NewScanAction.NEW_TABLE:
            self.examtable = ExamTable()
            self.current_exam_ids.clear()
            self.del_pages_area.clear()

        append_list_store(self.current_exam_ids, uniqe_exam_ids)

        if identified_failures is not None:
            self.examtable.insert_pages(identified_failures)

        self.examtable.insert_pages(successful_scans)
        self.build_examtable()

        self.parent.interactive_input_button.set_sensitive(True)
        self.parent.interactive_missing_input_button.set_sensitive(True)
        self.parent.csv_mapping_button.set_sensitive(True)
        self.parent.export_button.set_sensitive(True)

    def build_examtable(self):
        provider = Gtk.CssProvider()
        provider.load_from_data(
            """
            #page_button { padding: 3pt; }
            #duplicate_page_button { padding: 3pt; background: LightCoral;}
            #invalid_row { background: LightCoral; }
            """.encode()
        )
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        self.exam_rows = []
        for elem in self.matching_list.get_children():
            self.matching_list.remove(elem)

        self.matching_list.add(self.header_row)
        sep_row = Gtk.ListBoxRow()
        sep_row.add(Gtk.Separator())
        self.matching_list.add(sep_row)

        for exam in sorted(self.examtable.exams(), key=lambda exam: exam.exam_id):
            examrow = ExamListRow(exam, self, self.student_id_suggestions)
            self.exam_rows.append(examrow)
            self.matching_list.add(examrow.row)

        self.update_exam_count_label()

        self.matching_list.show_all()

        for page in self.deleted_pages:
            self.add_to_invalid_pages(page)

        self.update_page_nr_highlight([])

    def update_page(
        self, old_exam_id: str, old_page: ExamPage, new_data: NewData
    ) -> ScannedPage:
        """Updates old_page in the examtable by a new page with new_data"""
        self.remove_page(old_page, old_exam_id)
        new_page = ScannedPage(new_data.exam_id, new_data.page_nr, old_page.pdf)
        if new_data.deleted:
            self.add_to_invalid_pages(new_page)
        else:
            self.insert_page(new_page)
        return new_page

    def remove_page(self, page: ExamPage, exam_id: str):
        """Removes a page from the examtable and the corresponding exam row"""
        old_rows = [row for row in self.exam_rows if row.exam.contains(page)]
        assert len(old_rows) == 1
        old_row = old_rows[0]

        remaining_pages = old_row.remove_page(page)
        if remaining_pages == 0:
            self.matching_list.remove(old_row.row)
            self.exam_rows.remove(old_row)
            self.matching_list.show_all()

        self.update_exam_count_label()
        self.examtable.remove_page(exam_id, page)

    def insert_page(self, page: ScannedPage):
        """Inserts a page into the examtable and the corresponding exam row"""
        self.examtable.insert_page(page)

        # insert updated page into correct exam_row
        new_rows = [row for row in self.exam_rows if row.exam.exam_id == page.exam_id]
        if len(new_rows) == 0:
            # Create new examrow
            new_exams = [
                exam for exam in self.examtable.exams() if exam.exam_id == page.exam_id
            ]
            assert len(new_exams) == 1
            new_exam = new_exams[0]
            append_list_store(self.current_exam_ids, [new_exam.exam_id])
            examrow = ExamListRow(new_exam, self, self.student_id_suggestions)
            self.exam_rows.append(examrow)
            self.matching_list.add(examrow.row)
            self.matching_list.show_all()
        elif len(new_rows) == 1:
            new_rows[0].add_page(ExamPage.from_scanned_page(page))
        else:
            raise RuntimeError("Duplicate Exam Rows")
        self.update_exam_count_label()

    def add_to_invalid_pages(self, page: ScannedPage):
        self.del_pages_area.add_page(page)

    def on_select_csv(self, _widget):
        self.parent.mapping_menu_popover.popdown()
        mapping_dialog = CsvMappingDialog(self.parent.window)
        mapping_dialog.dialog.run()
        if mapping_dialog.csv is None:
            return

        with mapping_dialog.csv.open(newline="") as csv_f:
            dialect = csv.Sniffer().sniff(csv_f.read(1024))
            csv_f.seek(0)
            reader = csv.reader(csv_f, dialect=dialect)
            if mapping_dialog.contains_header:
                # Skip first line
                reader.__next__()
            mapping = {}
            for r in reader:
                # TODO: Check valid mapping
                mapping[r[mapping_dialog.exam_id_column]] = r[
                    mapping_dialog.student_id_column
                ]
            for exam in self.examtable.exams():
                try:
                    exam.student_id = mapping[exam.exam_id]
                except KeyError:
                    info(f"No mapping for exam {exam.exam_id}")

            self.build_examtable()

    def interactive_student_id_input(self, _widget, input_all):
        self.parent.mapping_menu_popover.popdown()
        for exam in self.examtable.exams():
            if input_all or exam.student_id is None or exam.student_id == "":
                dialog = InteractiveStudentIdInput(
                    ScannedPage.from_exam_page(exam.exam_id, exam.first_page()),
                    exam.student_id,
                    self.student_id_suggestions,
                )
                response = dialog.run()

                if response == Gtk.ResponseType.OK:
                    new_student_id = int(dialog.student_id_entry.get_text())
                    self.examtable.update_student_id(exam.exam_id, new_student_id)
                else:
                    dialog.destroy()
                    self.build_examtable()
                    break

                dialog.destroy()
                self.build_examtable()

    def update_page_nr_highlight(self, _widget):
        nr_pages = get_content(self.nr_pages_entry, int)
        if nr_pages is not None:
            for row in self.exam_rows:
                row.update_pagecnt_highlight(nr_pages)

    def export_clicked(self, _widget):
        nr_pages = get_content(self.nr_pages_entry, int)
        (state, exam_id) = self.examtable.check_validity(nr_pages)
        if state != ExamTableState.VALID:
            if (
                warn_export_dialog(self.parent.window, state, exam_id)
                != Gtk.ResponseType.OK
            ):
                return
        ExportDialog(self.parent.window, self.examtable)

    @Gtk.Template.Callback()
    def on_add_student_id_list_clicked(self, _widget):
        student_id_dialog = StudentIdListDialog(self.parent.window)
        student_id_dialog.dialog.run()
        if student_id_dialog.csv is None:
            return

        with student_id_dialog.csv.open(newline="") as csv_f:
            dialect = csv.Sniffer().sniff(csv_f.read(1024))
            csv_f.seek(0)
            reader = csv.reader(csv_f, dialect=dialect)
            if student_id_dialog.contains_header:
                # Skip first line
                reader.__next__()
            self.student_id_suggestions = Gtk.ListStore(str)
            for row in reader:
                student_id = row[student_id_dialog.column].strip()
                self.student_id_suggestions.append(
                    [
                        student_id,
                    ]
                )
        self.autocompl_button.get_child().set_text("Autocompletion\nActive")
        self.del_autocompl_button.set_sensitive(True)
        self.autocompl_popover.popdown()
        self.build_examtable()

    @Gtk.Template.Callback()
    def on_autocompl_delete_clicked(self, _widget):
        self.student_id_suggestions = Gtk.ListStore(str)
        self.build_examtable()
        self.autocompl_button.get_child().set_text("Autocompletion\nInactive")
        self.del_autocompl_button.set_sensitive(False)
        self.autocompl_popover.popdown()

    def update_exam_count_label(self):
        count = len(self.exam_rows)
        self.exam_count_label.set_text(f"Exams: {count}")

    def show_pdf(
        self,
        pdf,
        exam_id: Optional[str],
        page_nr: Optional[int],
        deleted: bool,
        callback,
        *cb_args,
    ):
        self.exam_parameter_box.activate(exam_id, page_nr, deleted, callback, *cb_args)

        self.pdf_view.set_pdf(pdf)


def is_in_list_store(store: Gtk.ListStore, key: str) -> bool:
    for row in store:
        if row[0] == key:
            return True
    return False


def append_list_store(store: Gtk.ListStore, items: Iterable):
    for i in items:
        if not is_in_list_store(store, i):
            store.append(
                [
                    i,
                ]
            )


ButtonPage = TypeVar("ButtonPage", ExamPage, ScannedPage)


class PageButtonBox(Gtk.HBox, Generic[ButtonPage]):
    buttonlist: List[tuple[ButtonPage, Gtk.Button]]

    def __init__(self, highlight_duplicate_pages: bool = False):
        super(PageButtonBox, self).__init__(homogeneous=False, spacing=0)

        self.set_halign = Gtk.Align.START
        self.get_style_context().add_class("linked")
        self.buttonlist = []
        self.highlight_duplicate_pages = highlight_duplicate_pages

    def add_page(self, page: ButtonPage, nr: int, callback, *cb_args):
        pagebutton = Gtk.Button.new_with_label(str(nr))
        pagebutton.connect("clicked", callback, *cb_args)
        pagebutton.set_name("page_button")

        for b in self.buttonlist:
            if b[0] == page:
                # The page is already in this button list
                return

        self.add(pagebutton)
        if len(self.get_children()) != 0:
            pos = 0
            while int(self.get_children()[pos].get_label()) < nr:
                pos += 1
            self.reorder_child(pagebutton, pos)
        self.show_all()
        self.buttonlist.append((page, pagebutton))
        self.highlight_duplicates()

    def highlight_duplicates(self):
        if self.highlight_duplicate_pages:
            for but in self.buttonlist:
                nr_dupes = sum(
                    1 for b in self.buttonlist if b[0].page_nr == but[0].page_nr
                )
                if nr_dupes == 1:
                    but[1].set_name("page_button")
                else:
                    but[1].set_name("duplicate_page_button")

    def remove_page(self, page: ButtonPage) -> int:
        """Removes the given page from this row. Returns the remaining exams in
        this row so that the parent structure can handle the row accordingly"""
        buttons = [b for b in self.buttonlist if b[0] == page]
        assert len(buttons) == 1
        self.remove(buttons[0][1])
        self.buttonlist.remove(buttons[0])
        self.highlight_duplicates()
        return len(self.buttonlist)

    def nr_pages(self) -> int:
        return len(self.buttonlist)

    def clear(self):
        for p, b in self.buttonlist:
            self.remove(b)
            self.buttonlist.remove((p, b))


class ExamListRow:
    def __init__(
        self,
        exam: Exam,
        parent: ScanSorter,
        completion_model: Optional[Gtk.ListStore] = None,
    ):
        self.exam: Exam = exam  # Reference to examdata for manipulation
        self.parent: ScanSorter = parent

        box = Gtk.HBox.new(homogeneous=False, spacing=6)
        box.set_halign(Gtk.Align.START)

        # Label
        label = Gtk.Label.new(str(self.exam.exam_id))
        label.set_can_focus(False)
        label.set_halign(Gtk.Align.CENTER)
        label.set_size_request(150, -1)
        box.pack_start(label, False, True, 10)

        # Student ID Entry
        self.student_id_entry = Gtk.Entry.new()
        if self.exam.student_id is not None:
            self.student_id_entry.set_text(str(self.exam.student_id))
        self.student_id_entry.set_alignment(1)
        self.student_id_entry.set_max_length(9)
        self.student_id_entry.set_size_request(150, -1)
        self.student_id_entry.set_alignment(0.5)
        self.student_id_entry.connect("changed", self.student_id_changed)
        box.pack_start(self.student_id_entry, False, True, 10)

        if completion_model is not None:
            completion = Gtk.EntryCompletion()
            completion.set_model(completion_model)
            completion.set_text_column(0)
            self.student_id_entry.set_completion(completion)

        # Page buttons
        self.page_butt_box: PageButtonBox = PageButtonBox(
            highlight_duplicate_pages=True
        )
        for page in sorted(self.exam.pages, key=lambda page: int(page.page_nr)):
            self.page_butt_box.add_page(page, page.page_nr, self.show_pdf, page)
        box.pack_start(self.page_butt_box, False, False, 10)

        self.row: Gtk.ListBoxRow = Gtk.ListBoxRow.new()
        self.row.set_focus_child(self.student_id_entry)
        self.row.add(box)

    def update_student_id(self, new_student_id: int):
        self.exam.student_id = new_student_id
        self.student_id_entry.set_text(str(new_student_id))

    def add_page(self, page: ExamPage):
        self.page_butt_box.add_page(page, page.page_nr, self.show_pdf, page)

    def remove_page(self, page: ExamPage) -> int:
        return self.page_butt_box.remove_page(page)

    def update_pagecnt_highlight(self, nr_pages: Optional[int]):
        if nr_pages is not None and self.page_butt_box.nr_pages() != nr_pages:
            self.row.set_name("invalid_row")
        else:
            self.row.set_name("")

    def student_id_changed(self, widget):
        new_student_id = get_content(widget, int)
        if new_student_id is not None:
            self.exam.student_id = new_student_id

    def show_pdf(self, _widget, page: ExamPage):
        self.parent.show_pdf(
            page.pdf, self.exam.exam_id, page.page_nr, False, self.pdf_updated_cb, page
        )

    def pdf_updated_cb(self, new_data: Optional[NewData], page: ExamPage):
        if new_data is not None:
            new_page = self.parent.update_page(self.exam.exam_id, page, new_data)
            self.parent.update_page_nr_highlight([])
        self.show_pdf([], ExamPage.from_scanned_page(new_page))


class DeletedPagesArea:
    def __init__(
        self, parent: ScanSorter, del_pages_area: Gtk.HBox, del_pages_vp: Gtk.Viewport
    ):
        self.parent: ScanSorter = parent
        self.main_box: Gtk.HBox = del_pages_area  # The Label and the box with the pages
        self.main_box.set_visible(False)
        self.vp = del_pages_vp

        self.pages_box: PageButtonBox = PageButtonBox(highlight_duplicate_pages=False)
        self.vp.add(self.pages_box)

    def add_page(self, page: ScannedPage):
        index = len(self.pages_box.buttonlist)
        self.pages_box.add_page(page, index, self.show_pdf, page)

        self.main_box.set_visible(True)

    def remove_page(self, page: ScannedPage):
        self.pages_box.remove_page(page)
        if self.pages_box.nr_pages() == 0:
            self.main_box.set_visible(False)

    def show_pdf(self, _widget, page: ScannedPage):
        self.parent.show_pdf(
            page.pdf, None, page.page_nr, True, self.pdf_updated_cb, page
        )

    def pdf_updated_cb(self, new_data: Optional[NewData], page: ScannedPage):
        if new_data is not None:
            if new_data.deleted:
                return
            self.remove_page(page)
            new_page = ScannedPage(new_data.exam_id, new_data.page_nr, page.pdf)
            self.parent.insert_page(new_page)
            self.parent.update_page_nr_highlight([])

    def clear(self):
        self.vp.remove(self.pages_box)
        self.pages_box: PageButtonBox = PageButtonBox(highlight_duplicate_pages=False)
        self.vp.add(self.pages_box)


class NewScanAction(IntEnum):
    APPEND = 1
    NEW_TABLE = 2


def confirm_new_scan(parent_window):
    confirm_dialog = Gtk.MessageDialog(
        parent_window,
        Gtk.DialogFlags.MODAL
        | Gtk.DialogFlags.DESTROY_WITH_PARENT
        | Gtk.DialogFlags.USE_HEADER_BAR,
        Gtk.MessageType.QUESTION,
        (
            "Cancel",
            Gtk.ResponseType.CANCEL,
            "Discard",
            NewScanAction.NEW_TABLE,
            "Keep",
            NewScanAction.APPEND,
        ),
        "How to handle already scanned exams?",
    )
    confirm_dialog.get_widget_for_response(
        NewScanAction.NEW_TABLE
    ).get_style_context().add_class("destructive-action")
    confirm_dialog.get_widget_for_response(
        NewScanAction.APPEND
    ).get_style_context().add_class("suggested-action")
    confirm_dialog.show_all()
    response = confirm_dialog.run()
    confirm_dialog.destroy()
    return response
