#!/usr/bin/env python

from exam_scan_tools import MainWindow
from gi.repository import Gtk


def start():
    MainWindow()

    Gtk.main()

    exit(0)
