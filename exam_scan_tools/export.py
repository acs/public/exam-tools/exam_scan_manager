from concurrent.futures import CancelledError, ThreadPoolExecutor
from enum import Enum
from io import BytesIO
from logging import error
from pathlib import Path
from threading import Thread
from typing import BinaryIO, Dict, Optional, cast

from gi.repository import GLib, Gtk
from PyPDF2 import PageObject, PdfFileReader, PdfFileWriter
from wand.drawing import Color, Drawing

from .exam import Exam, ExamTable, ExamTableState
from .gui_helpers import successful_with_open_folder_dialog


def warn_export_dialog(parent_window, state: ExamTableState, exam_id: Optional[str]):
    message = ""
    if state == ExamTableState.EMPTY:
        message = "Examtable is empty"
    elif state == ExamTableState.MISSING_ID:
        message = f"Exam {exam_id} is missing the student id"
    elif state == ExamTableState.WRONG_PAGECNT:
        message = f"Exam {exam_id} has an invalid pagecount"

    confirm_dialog = Gtk.MessageDialog(
        parent_window,
        Gtk.DialogFlags.MODAL
        | Gtk.DialogFlags.DESTROY_WITH_PARENT
        | Gtk.DialogFlags.USE_HEADER_BAR,
        Gtk.MessageType.QUESTION,
        (
            "Cancel",
            Gtk.ResponseType.CANCEL,
            "Export anyway",
            Gtk.ResponseType.OK,
        ),
        f"The examdata is inconsistent/incomplete:\n{message}",
    )
    confirm_dialog.show_all()
    response = confirm_dialog.run()
    confirm_dialog.destroy()
    return response


class ExportDialog:
    outdir: Optional[Path] = None

    def __init__(self, parent, examtable: ExamTable):
        gladefile = Path(__file__) / "../glade/Export_Dialog.glade"

        builder = Gtk.Builder()
        builder.add_from_file(str(gladefile.resolve()))

        self.dialog = builder.get_object("export_dialog")
        self.dialog.set_transient_for(parent)

        self.examtable = examtable

        self.export_button = builder.get_object("export_button")
        self.export_button.set_sensitive(False)
        self.export_button.connect("clicked", self.start_export)

        self.cancel_button = builder.get_object("cancel_button")
        self.cancel_button.connect("clicked", self.cancel_clicked)

        self.outdir_chooser = builder.get_object("outdir_chooser")
        self.outdir_chooser.connect("file-set", self.file_set)

        self.prefix_entry = builder.get_object("prefix_entry")

        self.settings_grid = builder.get_object("settings_grid")

        self.manip_pages_checkbox = builder.get_object("manip_pages_checkbox")
        self.manip_pages_checkbox.connect("toggled", self.manip_toggled)

        self.manip_revealer = builder.get_object("manip_revealer")
        self.manip_grid = builder.get_object("manip_grid")

        self.quality_combo = builder.get_object("quality_combo")
        self.watermark_checkbox = builder.get_object("watermark_checkbox")

        self.progress_revealer = builder.get_object("proc_reveal")
        self.progress_revealer.set_reveal_child(False)
        self.prog_box = builder.get_object("prog_box")
        self.proc_stack = builder.get_object("proc_stack")
        self.progress = builder.get_object("progress")
        self.export_label = builder.get_object("exports")

        self.canceled = False
        self.exported = 0
        self.total_pdf = 0
        self.export_thread: Optional[Thread] = None
        self.exporting = False
        self.dialog.run()

    def file_set(self, _widget):
        self.export_button.set_sensitive(True)
        self.export_button.get_style_context().add_class("suggested-action")

    def manip_toggled(self, _widget):
        reveal = self.manip_pages_checkbox.get_active()
        self.manip_revealer.set_reveal_child(reveal)
        self.manip_grid.set_visible(reveal)
        if not reveal:
            self.dialog.resize(10, 10)

    def cancel_clicked(self, _widget):
        if self.exporting:
            self.export_thread.cancel()
            self.canceled = True
            self.proc_stack.set_visible_child_name("canceling")
        else:
            self.dialog.destroy()

    def start_export(self, _widget):
        self.proc_stack.set_visible_child_name("processing")
        self.progress_revealer.set_reveal_child(True)
        self.cancel_button.get_style_context().add_class("destructive-action")
        self.export_button.get_style_context().remove_class("suggested-action")
        self.settings_grid.set_sensitive(False)
        self.export_button.set_sensitive(False)
        self.exporting = True
        self.prog_box.set_visible(True)

        self.outdir = Path(self.outdir_chooser.get_filename())
        prefix = self.prefix_entry.get_text()

        self.canceled = False
        Thread(target=self.main_export_thread, args=[prefix]).start()

    def main_export_thread(self, prefix: str):
        """
        This needs to be in a separate thread, with no Gtk interactions, so
        that the GUI doesn't block whilst waiting for the export to finish.
        """

        def callback():
            GLib.idle_add(self.update_progress)

        self.exported = 0
        self.total_pdf = sum(1 for _ in self.examtable.exams())
        self.export_label.set_text(f"Exported: {self.exported}/{self.total_pdf}")
        self.progress.set_fraction(0)

        if prefix is None:
            prefix = ""

        assert isinstance(self.outdir, Path)
        self.export_thread = ExportPDFThread(
            examtable=self.examtable,
            out_dir=self.outdir,
            quality=ExportQuality[self.quality_combo.get_active_id()],
            prefix=prefix,
            watermark=self.watermark_checkbox.get_active(),
            callback=callback,
        )
        self.export_thread.start()
        self.export_thread.join()

        if self.canceled:
            GLib.idle_add(self.after_cancel)
        else:
            GLib.idle_add(self.after_export)

    def update_progress(self):
        self.exported += 1
        self.progress.set_fraction(self.exported / self.total_pdf)
        self.export_label.set_text(f"Exported: {self.exported}/{self.total_pdf}")

    def after_export(self):
        self.exporting = False
        assert isinstance(self.outdir, Path)
        successful_with_open_folder_dialog(
            self.dialog, "Export successful", self.outdir
        )
        self.dialog.destroy()

    def after_cancel(self):
        # After cancelling, we can assume that the options are still valid
        self.progress_revealer.set_reveal_child(False)
        self.cancel_button.get_style_context().remove_class("destructive-action")
        self.export_button.get_style_context().add_class("suggested-action")
        self.settings_grid.set_sensitive(True)
        self.export_button.set_sensitive(True)
        self.exporting = False
        self.prog_box.set_visible(False)
        self.dialog.resize(10, 10)


class ExportQuality(Enum):
    NATIVE = 0
    HIGH = 1
    MEDIUM = 2
    LOW = 3

    def get_dpi_quality(self, native_dpi: int) -> tuple[int, int]:
        """Returns the output DPI and Compression Quality for the according level"""
        return {
            ExportQuality.NATIVE: (native_dpi, 95),
            ExportQuality.HIGH: (int(native_dpi * 0.75), 90),
            ExportQuality.MEDIUM: (int(native_dpi * 0.5), 85),
            ExportQuality.LOW: (int(native_dpi * 0.25), 85),
        }[self]


def get_dpi(page: PageObject) -> int:
    """Calculates the DPI from a pdf which must only contain a single image"""
    xobject = cast(Dict, page)["/Resources"]["/XObject"].getObject()
    for obj in xobject:
        if xobject[obj]["/Subtype"] == "/Image":
            width_px = xobject[obj]["/Width"]
            width_inches = page.mediaBox.getWidth() / 72
            w_dpi = int(width_px / width_inches)
            height_px = xobject[obj]["/Height"]
            height_inches = page.mediaBox.getHeight() / 72
            h_dpi = int(height_px / height_inches)
            assert abs(w_dpi - h_dpi < int(w_dpi / 10))
            return w_dpi
    raise RuntimeError("pdf doesn't contain a suitable image")


def exam_export_pdf(
    exam: Exam,
    outfile: BinaryIO,
    quality: ExportQuality,
    watermark: bool = False,
):
    writer = PdfFileWriter()
    for page in sorted(exam.pages, key=lambda page: int(page.page_nr)):
        pdf_page = page.pdf.get_reader_page()

        if quality == ExportQuality.NATIVE and not watermark:
            writer.addPage(pdf_page)
        else:
            native_dpi = get_dpi(pdf_page)
            (dpi, compr_quality) = quality.get_dpi_quality(native_dpi)

            compressed_img = BytesIO()
            with page.pdf.get_page_as_image(resolution=dpi) as img:
                # img.alpha_channel = "background"
                if watermark:
                    with Drawing() as watermark_text:
                        watermark_text.font_size = 200
                        textcolor = Color("rgb(200, 200, 200)")
                        textcolor.alpha = 0.1
                        watermark_text.fill_color = textcolor
                        watermark_text.text_alignment = "center"
                        watermark_text.translate(
                            int(img.size[0] / 2), int(img.size[1] / 2)
                        )
                        watermark_text.rotate(-60)
                        watermark_text.text(
                            0, int(watermark_text.font_size / 2), str(exam.student_id)
                        )
                        watermark_text.draw(img)

                img.compression = "jpeg"
                img.compression_quality = compr_quality
                img.save(compressed_img)

            compressed_img.seek(0)
            compressed_pdf = PdfFileReader(compressed_img)
            writer.addPage(compressed_pdf.getPage(0))

    writer.write(cast(BytesIO, outfile))


class ExportPDFThread(Thread):
    def __init__(
        self,
        examtable: ExamTable,
        out_dir: Path,
        quality: ExportQuality,
        prefix: str = "",
        watermark: bool = False,
        callback=None,
    ):
        Thread.__init__(self)
        self.examtable = examtable
        self.out_dir = out_dir
        self.quality = quality
        self.prefix = prefix
        self.watermark = watermark
        self.callback = callback
        self.executor = ThreadPoolExecutor()

    def cancel(self):
        self.executor.shutdown(wait=False, cancel_futures=True)

    def run(self):
        try:
            list(self.executor.map(self.export_exam, self.examtable.exams()))
        except (CancelledError, RuntimeError):
            return None

    def export_exam(self, exam: Exam):
        if exam.student_id is None:
            error(f"Exam {exam.exam_id} has no student id. Can't write file.")
            return
        with (self.out_dir / f"{self.prefix}{str(exam.student_id)}.pdf").open(
            "wb"
        ) as out_file:
            exam_export_pdf(exam, out_file, self.quality, self.watermark)
            if self.callback is not None:
                self.callback()
