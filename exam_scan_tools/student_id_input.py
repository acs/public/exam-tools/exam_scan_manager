from typing import Optional

from gi.repository import Gtk

from .exam import ScannedPage
from .gui_helpers import get_content
from .pdf_view import PdfView


class InteractiveStudentIdInput(Gtk.Dialog):
    def __init__(
        self,
        scanned_page: ScannedPage,
        prev_student_id: Optional[int] = None,
        completion_model: Optional[Gtk.ListStore] = None,
    ):
        Gtk.Dialog.__init__(
            self,
            title=f"Exam {scanned_page.exam_id}",
            flags=Gtk.DialogFlags.USE_HEADER_BAR
            | Gtk.DialogFlags.DESTROY_WITH_PARENT
            | Gtk.DialogFlags.MODAL,
        )

        self.add_buttons(
            Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE, Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        box = Gtk.Box.new(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)

        label = Gtk.Label.new("Student ID:")
        label.set_can_focus(False)
        label.set_halign(Gtk.Align.START)
        # label.set_size_request(150, -1)
        box.pack_start(label, False, False, 10)

        self.student_id_entry = Gtk.Entry.new()
        self.student_id_entry.set_alignment(0.5)
        self.student_id_entry.set_max_length(10)
        self.student_id_entry.set_placeholder_text("e.g. 123456")
        self.student_id_entry.set_size_request(150, -1)
        if prev_student_id is not None:
            self.student_id_entry.set_text(str(prev_student_id))

        if completion_model is not None:
            completion = Gtk.EntryCompletion()
            completion.set_model(completion_model)
            completion.set_text_column(0)
            self.student_id_entry.set_completion(completion)

        self.student_id_entry.connect("changed", self.change)
        self.student_id_entry.connect("activate", self.close)
        box.pack_start(self.student_id_entry, False, False, 10)

        self.set_default_size(800, 900)

        pdfview = PdfView(scanned_page.pdf)

        box2 = Gtk.Box.new(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        box2.set_halign = Gtk.Align.CENTER
        box2.set_hexpand(False)
        box2.pack_start(box, True, False, 0)

        self.get_content_area().pack_start(pdfview, True, True, 0)
        self.get_content_area().pack_start(box2, False, False, 0)

        # self.get_widget_for_response(Gtk.ResponseType.CANCEL).get_style_context().add_class("destructive-action")
        self.get_widget_for_response(Gtk.ResponseType.OK).get_style_context().add_class(
            "suggested-action"
        )

        self.change(None)
        self.show_all()

    def change(self, widget):
        student_id = get_content(self.student_id_entry, int)
        self.get_widget_for_response(Gtk.ResponseType.OK).set_sensitive(
            student_id is not None
        )

    def close(self, widget):
        self.response(Gtk.ResponseType.OK)
