import threading
from datetime import datetime, timedelta
from itertools import chain
from logging import info
from pathlib import Path
from typing import List, Optional, Tuple

from gi.repository import GLib, Gtk

from .barcode_scan import CodePlacement, ProcessPDFBinariesThread, file_to_pagerange
from .exam import ScannedPage


class NewScan:
    def __init__(self, parent):
        gladefile = Path(__file__) / "../glade/New_Scan.glade"
        builder = Gtk.Builder()
        builder.add_from_file(str(gladefile.resolve()))

        self.dialog = builder.get_object("new_scan")
        self.dialog.set_transient_for(parent)

        self.apply_button = builder.get_object("new_scan_apply")
        self.apply_button.connect("clicked", self.start_processing)

        self.file_select_button = builder.get_object("file_select_button")
        self.file_select_button.connect("clicked", self.file_selector_clicked)

        self.cancel_button = builder.get_object("new_scan_cancel")
        self.cancel_handler = self.cancel_button.connect("clicked", self.cancel)

        self.settings_grid = builder.get_object("scan_settings")
        self.dpi_entry = builder.get_object("dpi_entry")
        self.dpi_entry.connect("value-changed", self.settings_changed)

        self.barcode_location = builder.get_object("barcode_location")

        self.rescan = builder.get_object("rescan_toggle")

        self.revealer = builder.get_object("proc_reveal")
        self.revealer.set_reveal_child(False)
        self.prog_box = builder.get_object("prog_box")
        self.proc_stack = builder.get_object("proc_stack")
        self.progress = builder.get_object("progress")
        self.failure_label = builder.get_object("failures")

        self.failures: int = 0
        self.files = None
        self.dpi = self.dpi_entry.get_value_as_int()
        self.update_apply_button()

    def file_selector_clicked(self, widget):
        file_choose_dialog = Gtk.FileChooserDialog(
            "Open File",
            self.dialog,
            Gtk.FileChooserAction.OPEN,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.OK,
            ),
        )
        file_choose_dialog.set_select_multiple(True)
        pdf_filter = Gtk.FileFilter()
        pdf_filter.set_name("PDF Files")
        pdf_filter.add_mime_type("application/pdf")
        file_choose_dialog.add_filter(pdf_filter)

        file_choose_dialog.get_widget_for_response(
            Gtk.ResponseType.OK
        ).get_style_context().add_class("suggested-action")
        file_choose_dialog.get_widget_for_response(
            Gtk.ResponseType.CANCEL
        ).get_style_context().add_class("destructive-action")

        response = file_choose_dialog.run()
        if response == Gtk.ResponseType.OK:
            self.files = file_choose_dialog.get_filenames()
            if len(self.files) > 1:
                self.file_select_button.set_label(f"{len(self.files)} Files")
            else:
                self.file_select_button.set_label(f"{len(self.files)} File")
            self.update_apply_button()
        else:
            self.files = None
            self.file_select_button.set_label("Select Files")
            self.update_apply_button()
        file_choose_dialog.destroy()

    def run(self) -> Optional[Tuple[List[ScannedPage], List[ScannedPage]]]:
        response = self.dialog.run()
        self.dialog.destroy()
        if response == Gtk.ResponseType.OK:
            return (self.failure_list, self.identified_pdfs)
        elif response == Gtk.ResponseType.CANCEL:
            return None
        else:
            raise RuntimeError("Invalid Response")

    def cancel(self, widget):
        self.dialog.response(Gtk.ResponseType.CANCEL)

    def update_apply_button(self):
        if self.dpi is not None and self.files is not None:
            self.apply_button.set_sensitive(True)
        else:
            self.apply_button.set_sensitive(False)

    def settings_changed(self, widget):
        self.dpi = self.dpi_entry.get_value_as_int()
        self.update_apply_button()

    def cancel_scan(self, widget):
        if confirm_cancel_dialog(self.dialog) != Gtk.ResponseType.OK:
            return
        self.canceled = True
        self.proc_stack.set_visible_child_name("canceling")
        self.pdf_scan_thread.cancel()
        self.cancel_button.set_sensitive(False)

    def start_processing(self, widget):
        self.proc_stack.set_visible_child_name("scanning")

        self.settings_grid.set_sensitive(False)

        self.revealer.set_reveal_child(True)
        self.prog_box.set_visible(True)

        self.cancel_button.disconnect(self.cancel_handler)
        self.cancel_handler = self.cancel_button.connect("clicked", self.cancel_scan)

        self.reset_progress()
        self.apply_button.set_sensitive(False)

        code_placement = CodePlacement[self.barcode_location.get_active_id()]
        threading.Thread(target=self.pdf_process_thread, args=[code_placement]).start()

    def pdf_process_thread(self, code_placement: CodePlacement):
        # This fn is a workaround for https://github.com/python/mypy/issues/9590
        def to_filerange(file):
            return file_to_pagerange(file)

        pagerange = list(chain(*map(to_filerange, self.files)))
        self.pagecount = len(pagerange)
        self.failure_label.set_text(f"Not recognized: 0/{self.pagecount}")

        self.canceled = False

        def callback(b):
            GLib.idle_add(self.update_progress, b)

        self.pdf_scan_thread = ProcessPDFBinariesThread(
            pagerange,
            dpi=self.dpi,
            callback=callback,
            rescan=self.rescan.get_active(),
            code_placement=code_placement,
        )

        self.scan_start = datetime.now()
        self.eta = timedelta(seconds=999999999)
        self.pdf_scan_thread.start()

        self.pdf_scan_thread.join()
        scanned_pages = self.pdf_scan_thread.returnval

        if scanned_pages is None or self.canceled is True:
            info("Canceled Scan")
            GLib.idle_add(self.after_failed_pdf_processing)
            return

        self.failure_list = []
        self.identified_pdfs = []
        for page in scanned_pages:
            if page.exam_id is None:
                self.failure_list.append(page)
            else:
                self.identified_pdfs.append(page)

        GLib.idle_add(self.after_pdf_processing)

    def after_failed_pdf_processing(self):
        self.settings_grid.set_sensitive(True)
        self.revealer.set_reveal_child(False)
        self.update_apply_button()
        self.prog_box.set_visible(False)
        self.dialog.resize(30, 20)

        self.cancel_button.set_sensitive(True)
        self.cancel_button.disconnect(self.cancel_handler)
        self.cancel_handler = self.cancel_button.connect("clicked", self.cancel)

    def after_pdf_processing(self):
        self.dialog.response(Gtk.ResponseType.OK)

    def update_progress(self, Success: bool):
        self.progress.set_fraction(self.progress.get_fraction() + 1 / self.pagecount)
        time_passed = datetime.now() - self.scan_start
        eta = (1 / self.progress.get_fraction() - 1) * time_passed
        # Going up with the eta looks really bad and only confuses the user...
        if eta < self.eta:
            self.eta = eta

        if not Success:
            self.failures += 1
        self.failure_label.set_text(
            f"Not recognized: {str(self.failures)}/{self.pagecount}\nETA: {self.eta.seconds + 1}s"
        )

    def reset_progress(self):
        self.progress.set_fraction(0)
        self.failures = 0
        self.failure_label.set_text("Not recognized: 0")


def confirm_cancel_dialog(parent_window):
    """
    Creates a dialog window which asks for confirmation upon canceling a critical operation.
    Returns `Gtk.ResponseType.OK` if the user wants to continue with the operation and `Gtk.ResponseType.CANCEL` if the user doesn't
    """
    confirm_dialog = Gtk.MessageDialog(
        parent_window,
        Gtk.DialogFlags.MODAL
        | Gtk.DialogFlags.DESTROY_WITH_PARENT
        | Gtk.DialogFlags.USE_HEADER_BAR,
        Gtk.MessageType.QUESTION,
        (
            "Cancel Scan",
            Gtk.ResponseType.OK,
            "Do nothing",
            Gtk.ResponseType.CANCEL,
        ),
        "Do you really want to cancel?",
    )
    confirm_dialog.format_secondary_text("All pages need to be rescanned if you cancel")
    confirm_dialog.get_widget_for_response(
        Gtk.ResponseType.OK
    ).get_style_context().add_class("destructive-action")
    confirm_dialog.get_widget_for_response(
        Gtk.ResponseType.CANCEL
    ).get_style_context().add_class("suggested-action")
    confirm_dialog.show_all()
    response = confirm_dialog.run()
    confirm_dialog.destroy()
    return response
