from enum import Enum
from io import BytesIO
from pathlib import Path
from typing import BinaryIO, Union

from gi.repository import Gio, Poppler
from PyPDF2 import PageObject, PdfFileReader, PdfFileWriter
from wand.image import Image


class Rotation(Enum):
    NORMAL = 0
    FLIPPED = 1

    def get_angle(self) -> int:
        """Calculate the angle required ot rotate the page back to normal"""
        if self == Rotation.NORMAL:
            return 0
        elif self == Rotation.FLIPPED:
            return 180
        else:
            raise RuntimeError("Invalid Rotation")


class MemoryPdf:
    """A PDF page provider that keeps the pdf as stream in memory for fast access"""

    def __init__(self, stream: BinaryIO):
        self.stream = stream

    def get_page(self):
        self.stream.seek(0)
        input_stream = Gio.MemoryInputStream.new_from_data(self.stream.read())
        return Poppler.Document.new_from_stream(input_stream, -1, None, None).get_page(
            0
        )

    def get_reader_page(self) -> PageObject:
        self.stream.seek(0)
        reader = PdfFileReader(self.stream)
        assert reader.getNumPages() == 1
        return reader.getPage(0)

    def get_page_as_image(self, resolution: int) -> Image:
        self.stream.seek(0)
        return Image(file=self.stream, resolution=resolution)


class LazyPdf:
    """A PDF page provider that reads the file on-demand from the disk"""

    def __init__(self, path: Path, page: int, rotation: Rotation = Rotation.NORMAL):
        self.path: Path = path
        self.page = page
        self.rotation = rotation

    def get_page(self):
        uri = self.path.resolve().as_uri()
        if self.rotation == Rotation.NORMAL:
            return Poppler.Document.new_from_file(uri).get_page(self.page)
        else:
            rotated_pdf = PdfFileWriter()
            rotated_pdf.addPage(self.get_reader_page())
            rotated_bytes = BytesIO()
            rotated_pdf.write(rotated_bytes)
            rotated_bytes.seek(0)
            input_stream = Gio.MemoryInputStream.new_from_data(rotated_bytes.read())
            return Poppler.Document.new_from_stream(
                input_stream, -1, None, None
            ).get_page(0)

    def get_reader_page(self) -> PageObject:
        reader = PdfFileReader(open(self.path, "rb"))
        page = reader.getPage(self.page)
        rotate_page(page, self.rotation)
        return page

    def get_page_as_image(self, resolution: int) -> Image:
        # https://stackoverflow.com/questions/55027503/python-wand-convert-only-first-pdf-page
        paged_filename = str(self.path) + "[" + str(self.page) + "]"
        img = Image(filename=paged_filename, resolution=resolution)
        if self.rotation != Rotation.NORMAL:
            img.rotate(self.rotation.get_angle())
        return img


def rotate_page(page: PageObject, rotation: Rotation):
    page.rotate_clockwise(rotation.get_angle())


default_pdf = LazyPdf(Path(__file__) / "../assets/Sleeping-Kitty.pdf", 0)

# Pseudo interface type representing a pdf provider with a get_page function:
PdfProvider = Union[MemoryPdf, LazyPdf]
