from enum import Enum
from logging import error
from pathlib import Path

from gi.repository import Gtk

from .barcode_generation_gui import BarcodeGeneratorBox
from .gui_helpers import show_about_dialog
from .scan_sorter import ScanSorter


class GuiPages(Enum):
    BARCODE_CREATON = 1
    SCAN_SORTING = 2


class MainWindow:
    def __init__(self):
        gladefile = Path(__file__) / "../glade/Main_Window.glade"
        self.builder = Gtk.Builder()
        self.builder.add_from_file(str(gladefile.resolve()))

        self.window = self.builder.get_object("window")

        self.main_stack = self.builder.get_object("main_stack")
        self.barcode_generation_box = BarcodeGeneratorBox(self)
        self.main_stack.add_titled(
            self.barcode_generation_box, "barcode_generation", "Barcode Generation"
        )
        self.scan_sorter = ScanSorter(self)
        self.main_stack.add_titled(self.scan_sorter, "scan_sorter", "Scan Sorting")

        self.window.show_all()
        handlers = {
            "onDestroy": Gtk.main_quit,
            "onCSVSelect": self.scan_sorter.on_select_csv,
            "onNewScanClicked": self.scan_sorter.new_scan,
            "on_about_clicked": self.on_about_clicked,
            "export_clicked": self.scan_sorter.export_clicked,
            "on_main_stack_visible_child_changed": self.on_main_stack_visible_child_changed,
        }
        self.builder.connect_signals(handlers)

        self.interactive_input_button = self.builder.get_object(
            "interactive_student_id_button"
        )
        self.interactive_input_button.connect(
            "clicked", self.scan_sorter.interactive_student_id_input, True
        )
        self.interactive_missing_input_button = self.builder.get_object(
            "interactive_missing_student_id_button"
        )
        self.interactive_missing_input_button.connect(
            "clicked", self.scan_sorter.interactive_student_id_input, False
        )

        self.mapping_menu_popover = self.builder.get_object("mapping_menu_popover")
        self.help_menu_popover = self.builder.get_object("help_menu_popover")

        self.csv_mapping_button = self.builder.get_object("csv_mapping_button")
        self.csv_mapping_button.set_sensitive(False)

        self.export_button = self.builder.get_object("export_button")
        self.new_scan_button = self.builder.get_object("new_scan_button")
        self.mapping_button = self.builder.get_object("mapping_button")

        self.set_visible_buttons(GuiPages.BARCODE_CREATON)

    def on_main_stack_visible_child_changed(self, stack_widget, variable):
        visible_page = stack_widget.get_visible_child_name()
        if visible_page == "scan_sorter":
            self.set_visible_buttons(GuiPages.SCAN_SORTING)
        elif visible_page == "barcode_generation":
            self.set_visible_buttons(GuiPages.BARCODE_CREATON)
        else:
            error(f"Invalid stack page: {visible_page}")
            raise (RuntimeError(f"Invalid stack page: {visible_page}"))

    def set_visible_buttons(self, page: GuiPages):
        if page == GuiPages.BARCODE_CREATON:
            self.export_button.set_visible(False)
            self.mapping_button.set_visible(False)
            self.new_scan_button.set_visible(False)
        elif page == GuiPages.SCAN_SORTING:
            self.export_button.set_visible(True)
            self.mapping_button.set_visible(True)
            self.new_scan_button.set_visible(True)

    def on_about_clicked(self, _widget):
        self.help_menu_popover.popdown()
        show_about_dialog(self.window)
