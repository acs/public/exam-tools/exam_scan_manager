from pathlib import Path
from typing import Iterable, List, Optional

from gi.repository import Gtk

from .exam import ScannedPage
from .examid_entry import ExamIdEntry
from .gui_helpers import get_content
from .pdf_view import PdfView


class ManualMatchingDialog:
    deleted_pages: List[ScannedPage] = []
    identified_failures: List[ScannedPage] = []
    manual_examid_matchers: List["ManualPDFMatcher"] = []

    def __init__(
        self,
        parent,
        failed_scans: Iterable[ScannedPage],
        exam_ids: Optional[Gtk.ListStore] = None,
    ):
        gladefile = Path(__file__) / "../glade/Manual_Matching_Dialog.glade"
        self.builder = Gtk.Builder()
        self.builder.add_from_file(str(gladefile.resolve()))
        handlers = {
            "done_clicked": self.done,
            "cancel_clicked": self.cancel,
            "skip_clicked": self.skip,
        }
        self.builder.connect_signals(handlers)

        self.dialog = self.builder.get_object("manual_matching_dialog")
        self.dialog.connect("close", self.esc_pressed)
        self.dialog.set_transient_for(parent)
        self.pages = self.builder.get_object("unmatched_pages")

        self.done_button = self.builder.get_object("done_button")
        self.skip_remaining = self.builder.get_object("skip_remaining")

        self.create_machter_stack(failed_scans, exam_ids)

    def create_machter_stack(
        self,
        failed_scans: Iterable[ScannedPage],
        exam_ids: Optional[Gtk.ListStore] = None,
    ):
        self.manual_examid_matchers = []
        for i, scanned_page in enumerate(failed_scans):
            scanned_page.exam_id = str(i)
            pdfview = ManualPDFMatcher(self, scanned_page, exam_ids)
            self.pages.append_page(pdfview, Gtk.Label(label="???"))
            self.manual_examid_matchers.append(pdfview)

        self.manual_examid_matchers[0].exam_id_entry.grab_focus()

    def run(self):
        response = self.dialog.run()
        self.dialog.destroy()
        if response == Gtk.ResponseType.OK:
            return (self.identified_failures, self.deleted_pages)
        elif response == Gtk.ResponseType.CANCEL:
            return None
        else:
            raise RuntimeError("Cancel on manual matching dialog")

    def update_manual_examid_view(self):
        self.identified_failures = []
        self.deleted_pages = []
        all_valid = True
        for i, m in enumerate(self.manual_examid_matchers):
            exam_page = m.get_values()
            current_tab = self.pages.get_nth_page(i)
            label = self.pages.get_tab_label(current_tab)
            if exam_page.exam_id == "DELETED-PAGE":
                self.deleted_pages.append(exam_page)
                label.set_text("invalid")
            elif exam_page.exam_id is None or exam_page.page_nr is None:
                self.deleted_pages.append(exam_page)
                label.set_text("???")
                all_valid = False
            else:
                label.set_text(f"{exam_page.exam_id}-{exam_page.page_nr}")
                self.identified_failures.append(exam_page)
        if all_valid:
            self.done_button.set_sensitive(True)
            self.skip_remaining.set_sensitive(False)
        else:
            self.done_button.set_sensitive(False)

    def cancel(self, widget):
        confirm_dialog = Gtk.MessageDialog(
            self.dialog,
            Gtk.DialogFlags.MODAL
            | Gtk.DialogFlags.DESTROY_WITH_PARENT
            | Gtk.DialogFlags.USE_HEADER_BAR,
            Gtk.MessageType.QUESTION,
            (
                "Yes",
                Gtk.ResponseType.OK,
                "No",
                Gtk.ResponseType.CANCEL,
            ),
            "Do you really want to cancel?",
        )
        confirm_dialog.format_secondary_text(
            "All pages need to be rescanned and all progress is lost"
        )
        confirm_dialog.get_widget_for_response(
            Gtk.ResponseType.OK
        ).get_style_context().add_class("destructive-action")
        confirm_dialog.get_widget_for_response(
            Gtk.ResponseType.CANCEL
        ).get_style_context().add_class("suggested-action")
        confirm_dialog.show_all()
        response = confirm_dialog.run()
        confirm_dialog.destroy()
        if response == Gtk.ResponseType.OK:
            self.dialog.response(Gtk.ResponseType.CANCEL)

    def done(self, widget):
        self.dialog.response(Gtk.ResponseType.OK)

    def skip(self, widget):
        self.update_manual_examid_view()
        self.dialog.response(Gtk.ResponseType.OK)

    def next_pdf(self, widget):
        self.pages.next_page()
        self.manual_examid_matchers[
            self.pages.get_current_page()
        ].exam_id_entry.grab_focus()

    def esc_pressed(self, widget):
        self.dialog.emit_stop_by_name("close")


class ManualPDFMatcher(Gtk.Box):
    page: ScannedPage

    def __init__(
        self, parent, page: ScannedPage, exam_ids: Optional[Gtk.ListStore] = None
    ):
        Gtk.Box.__init__(self)
        gladefile = Path(__file__) / "../glade/Manual_PDF_Matcher.glade"
        builder = Gtk.Builder()
        builder.add_from_file(str(gladefile.resolve()))

        self.components = builder.get_object("manual_matcher")
        self.pack_start(self.components, True, True, 0)

        self.page = page
        pdfview = PdfView(self.page.pdf)
        self.components.pack_start(pdfview, True, True, 10)

        self.del_button = builder.get_object("delete_button")
        self.pagenr_entry = builder.get_object("page_nr_entry")

        self.exam_id_entry = ExamIdEntry(exam_ids)
        builder.get_object("entry_box").pack_start(self.exam_id_entry, False, True, 0)

        self.parent = parent

        handlers = {
            "onDeleteToggle": self.delete,
            "onChanged": self.change,
        }
        builder.connect_signals(handlers)

        self.pagenr_entry.connect("activate", self.parent.next_pdf)
        self.exam_id_entry.connect("activate", self.focus_pagenr_entry)

        self.show_all()

    def focus_pagenr_entry(self, widget):
        self.pagenr_entry.grab_focus()

    def get_values(self):
        if self.del_button.get_active():
            return ScannedPage(exam_id="DELETED-PAGE", page_nr=None, pdf=self.page.pdf)
        return self.page

    def delete(self, widget):
        sensitive = not self.del_button.get_active()
        self.exam_id_entry.set_sensitive(sensitive)
        self.pagenr_entry.set_sensitive(sensitive)
        self.parent.next_pdf(widget)
        self.parent.update_manual_examid_view()

    def change(self, widget):
        self.page.exam_id = get_content(self.exam_id_entry, str)
        self.page.page_nr = get_content(self.pagenr_entry, int)
        # TODO: This is an ineffective pattern
        self.parent.update_manual_examid_view()
