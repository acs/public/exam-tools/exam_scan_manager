from __future__ import annotations

from io import BytesIO
from os import listdir
from pathlib import Path
from threading import Event, Thread
from typing import TYPE_CHECKING, Optional

from gi.repository import GLib, Gtk
from PyPDF2.errors import PdfReadError

from .barcode_creation import (
    BarcodeGeneratorThread,
    BarcodePlacement,
    Position,
    SeparationSettings,
    SplitMode,
    barcode_on_single_page,
    generate_ids,
    nr_pdf_pages,
    write_list_to_file,
)
from .gui_helpers import (
    get_content,
    get_content_list,
    successful_with_open_folder_dialog,
)
from .pdf import LazyPdf, MemoryPdf
from .pdf_view import PdfView

if TYPE_CHECKING:
    from .main_window import MainWindow


@Gtk.Template(
    filename=str((Path(__file__) / "../glade/Barcode_Generation.glade").resolve())
)
class BarcodeGeneratorBox(Gtk.Box):
    __gtype_name__ = "barcode_generation_box"

    barcode_size_spinner = Gtk.Template.Child("barcode_size_spinner")
    exam_nr_spinner = Gtk.Template.Child("exam_nr_spinner")
    gen_cancel_stack = Gtk.Template.Child("gen_cancel_stack")
    generate_button = Gtk.Template.Child("generate_button")
    input_pdf = Gtk.Template.Child("input_pdf")
    multi_entry = Gtk.Template.Child("multi_entry")
    offset_lr_spinner = Gtk.Template.Child("offset_lr_spinner")
    offset_tb_spinner = Gtk.Template.Child("offset_tb_spinner")
    output_pdf = Gtk.Template.Child("output_pdf")
    page_nr_adj = Gtk.Template.Child("page_nr_adj")
    pagenr_spinner = Gtk.Template.Child("pagenr_spinner")
    pdf_box = Gtk.Template.Child("pdf_box")
    position_combo = Gtk.Template.Child("position_combo")
    prog_revealer = Gtk.Template.Child("prog_revealer")
    progress = Gtk.Template.Child("progress")
    separation_selection = Gtk.Template.Child("separation_selection")
    settings_grid = Gtk.Template.Child("settings_grid")
    single_entry = Gtk.Template.Child("single_entry")
    split_checkbox = Gtk.Template.Child("split_checkbox")
    split_revealer = Gtk.Template.Child("split_revealer")
    stack = Gtk.Template.Child("separation_file_stack")

    def __init__(self, parent: MainWindow):
        super(Gtk.Box, self).__init__()
        self.parent = parent
        self.sep_list = None

        self.pdf_view = PdfView(
            LazyPdf((Path(__file__) / "../assets/barcode_gen_background.pdf"), 0)
        )
        self.pdf_view.set_size_request(400, 400)

        self.pdf_box.pack_start(self.pdf_view, True, True, 5)

        self.pdf_prev_event = Event()
        self.pdf_prev_event.clear()

        prev_thread = Thread(target=self.pdf_preview_thread, daemon=True)
        prev_thread.start()
        self.sep = None
        self.barcode_create_thread = None
        self.examcount = None
        self.out_folder: Optional[Path] = None

    @Gtk.Template.Callback()
    def on_setting_changed(self, _widget):
        filename = self.input_pdf.get_filename()
        # TODO: deactivate settings if file not set
        outfile_name = self.output_pdf.get_filename()
        if outfile_name is not None:
            self.out_folder = Path(outfile_name)
        else:
            self.out_folder = None
        position = self.position_combo.get_active_id()

        self.sep = None
        sep_setting_valid = True
        if self.split_checkbox.get_active():
            split_mode = SplitMode[self.separation_selection.get_active_id()]
            filename = None
            if self.out_folder is not None:
                if split_mode == SplitMode.SINGLE:
                    filename = get_content(self.single_entry, str)
                    filename = str((self.out_folder / filename).resolve())
                else:
                    filename = get_content(self.multi_entry, str)
                    filename = str((self.out_folder / filename).resolve())

            if self.sep_list is not None and filename is not None:
                self.sep = SeparationSettings(split_mode, self.sep_list, filename)
            else:
                sep_setting_valid = False

        self.generate_button.set_sensitive(
            filename is not None
            and self.out_folder is not None
            and position is not None
            and sep_setting_valid
        )

    @Gtk.Template.Callback()
    def on_update_page_spinner(self, _widget):
        filename = self.input_pdf.get_filename()
        if filename is not None:
            with open(filename, mode="rb") as file_handle:
                nr_pages = nr_pdf_pages(file_handle)
                self.page_nr_adj.set_upper(nr_pages)
            self.pagenr_spinner.set_sensitive(True)

    @Gtk.Template.Callback()
    def on_trigger_preview(self, _widget):
        self.pdf_prev_event.set()

    def pdf_preview_thread(self):
        while True:
            self.pdf_prev_event.wait()
            self.pdf_prev_event.clear()

            filename = self.input_pdf.get_filename()
            pos = self.position_combo.get_active_id()
            if filename is not None and pos is not None:
                with open(filename, mode="rb") as file_handle:
                    pdfstream = BytesIO()
                    placement = BarcodePlacement(
                        pos=Position[pos],
                        offset_lr=self.offset_lr_spinner.get_value(),
                        offset_tb=self.offset_tb_spinner.get_value(),
                        height=self.barcode_size_spinner.get_value(),
                    )
                    try:
                        barcode_on_single_page(
                            exam_file=file_handle,
                            page_nr=self.pagenr_spinner.get_value_as_int() - 1,
                            output_stream=pdfstream,
                            placement=placement,
                        )
                        pdfstream.seek(0)
                        pdf_preview = MemoryPdf(pdfstream)
                        GLib.idle_add(self.pdf_view.set_pdf, pdf_preview)
                    except PdfReadError:
                        GLib.idle_add(self.pdf_read_failed)

    def pdf_read_failed(self):
        self.input_pdf.unselect_all()
        warn_dialog = Gtk.MessageDialog(
            parent=self.parent.window,
            flags=Gtk.DialogFlags.MODAL,
            type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.OK,
            message_format="Could not read pdf",
        )
        warn_dialog.run()
        warn_dialog.destroy()

    @Gtk.Template.Callback()
    def on_split_pages_clicked(self, _widget):
        self.split_revealer.set_reveal_child(self.split_checkbox.get_active())

    @Gtk.Template.Callback()
    def on_separation_changed(self, widget):
        self.stack.set_visible_child_name(widget.get_active_id())

    @Gtk.Template.Callback()
    def on_update_sep_list(self, widget):
        self.sep_list = get_content_list(widget, int)

    @Gtk.Template.Callback()
    def on_generate_clicked(self, _widget):
        if len(listdir(Path(self.output_pdf.get_filename()))):
            warn_dialog = Gtk.MessageDialog(
                self.parent.window,
                Gtk.DialogFlags.MODAL
                | Gtk.DialogFlags.DESTROY_WITH_PARENT
                | Gtk.DialogFlags.USE_HEADER_BAR,
                type=Gtk.MessageType.WARNING,
                buttons=Gtk.ButtonsType.OK_CANCEL,
                message_format="Output directory is not empty, continue?",
            )
            warn_dialog.format_secondary_text("This might overwrite existing files.")
            response = warn_dialog.run()
            warn_dialog.destroy()
            if response != Gtk.ResponseType.OK:
                return

        self.prog_revealer.set_reveal_child(True)
        self.settings_grid.set_sensitive(False)
        self.gen_cancel_stack.set_visible_child_name("cancel")

        filename = self.input_pdf.get_filename()
        pos = self.position_combo.get_active_id()
        assert filename is not None and pos is not None

        self.progress.set_fraction(0)
        Thread(target=self.generation_thread).start()

    def generation_thread(self):
        pos = self.position_combo.get_active_id()
        filename = self.input_pdf.get_filename()
        output_folder = self.output_pdf.get_filename()
        exam_ids = generate_ids(self.exam_nr_spinner.get_value_as_int())
        self.examcount = len(exam_ids)
        placement = BarcodePlacement(
            pos=Position[pos],
            offset_lr=self.offset_lr_spinner.get_value(),
            offset_tb=self.offset_tb_spinner.get_value(),
            height=self.barcode_size_spinner.get_value(),
        )
        self.barcode_create_thread = BarcodeGeneratorThread(
            exam_file_path=Path(filename),
            output_folder=Path(output_folder),
            exam_ids=exam_ids,
            placement=placement,
            separation=self.sep,
            callback=lambda: GLib.idle_add(self.update_progress),
        )

        with (Path(output_folder) / "generated_ids.csv").open("w") as csv_file:
            write_list_to_file(exam_ids, csv_file)

        self.barcode_create_thread.start()
        self.barcode_create_thread.join()
        GLib.idle_add(self.after_generation)

    def update_progress(self):
        self.progress.set_fraction(self.progress.get_fraction() + 1 / self.examcount)

    def after_generation(self):
        self.prog_revealer.set_reveal_child(False)
        self.settings_grid.set_sensitive(True)
        self.gen_cancel_stack.set_visible_child_name("generate")
        successful_with_open_folder_dialog(
            self.parent.window, "Exam generation successful", self.out_folder
        )

    @Gtk.Template.Callback()
    def on_cancel_clicked(self, _widget):
        self.barcode_create_thread.cancel()
        self.after_generation()
