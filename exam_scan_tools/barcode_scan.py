import concurrent.futures
import re
import threading
from enum import Enum
from io import BytesIO
from pathlib import Path
from typing import BinaryIO, Iterable, List, Sequence, Tuple

import wand.image
from PIL import Image
from PyPDF2 import PageObject, PdfFileReader, PdfFileWriter
from pyzbar.pyzbar import decode

from .exam import ScannedPage
from .pdf import LazyPdf, MemoryPdf, PdfProvider, Rotation


class CodePlacement(Enum):
    """Placement of a QR/Barcode on a page."""

    BOTTOM = 0
    TOP = 1


def halve_image(image: Image, code_placement: CodePlacement):
    """Returns only the half of the image where the QR/Barcode is supposed to be"""
    (width, height) = (image.width, image.height)
    if code_placement == CodePlacement.TOP:
        area = (0, 0, width, height / 2)
    elif code_placement == CodePlacement.BOTTOM:
        area = (0, height / 2, width, height)
    else:
        raise RuntimeError("Invalid barcode location")
    return image.crop(area)


def extraxt_qr_text(page_pdf: PdfProvider, dpi: int = 250) -> Tuple[str, CodePlacement]:
    """looks for a QR or barcode on the respective PDF page"""
    for placement in [CodePlacement.BOTTOM, CodePlacement.TOP]:
        # PDF to jpg
        img = page_pdf.get_page_as_image(dpi)
        img.alpha_channel = "background"
        jpg = img.convert("jpg")
        jpg_bytes = BytesIO()
        jpg.save(file=jpg_bytes)
        jpg_bytes.seek(0)

        pil_img = Image.open(jpg_bytes)
        cropped_image = halve_image(pil_img, placement)
        code = decode(cropped_image)
        pil_img.close()
        jpg_bytes.close()
        if len(code) == 1:
            return (code[0].data.decode(encoding="utf-8", errors="strict"), placement)

    raise RuntimeError("Can't parse Barcode")


exam_id_regex = re.compile(r"(?:.*-)*(\w+)-(\d+)")


def process_pdf_page(
    pdf_prov: PdfProvider, dpi: int, placement: CodePlacement
) -> ScannedPage:
    """Scans a PDF page for the exam id and the page number based on the bar/QR code"""
    try:
        (barcode_text, code_location) = extraxt_qr_text(pdf_prov, dpi)
        (exam_id, pagenr) = exam_id_regex.findall(barcode_text)[0]

        if isinstance(pdf_prov, MemoryPdf):
            if code_location != placement:
                rotated_pdf = PdfFileWriter()
                page = pdf_prov.get_reader_page()
                page.rotateClockwise(180)
                rotated_pdf.addPage(page)
                rotated_bytes = BytesIO()
                rotated_pdf.write(rotated_bytes)
                return ScannedPage(exam_id, int(pagenr), MemoryPdf(rotated_bytes))
            else:
                return ScannedPage(exam_id, int(pagenr), pdf_prov)
        elif isinstance(pdf_prov, LazyPdf):
            if code_location != placement:
                pdf_prov.rotation = Rotation.FLIPPED
            else:
                pdf_prov.rotation = Rotation.NORMAL
            return ScannedPage(exam_id, int(pagenr), pdf_prov)

    except RuntimeError:
        return ScannedPage(None, None, pdf_prov)


def file_to_pagerange(filename: str) -> Sequence[PdfProvider]:
    """
    Opens a pdf file and returns a range over the pages
    """
    input_pdf = PdfFileReader(open(filename, "rb"))
    nr_pages = len(input_pdf.pages)
    pagerange = [LazyPdf(Path(filename), p) for p in range(0, nr_pages)]
    return pagerange


class ProcessPDFBinariesThread(threading.Thread):
    """
    Scans all pages of the file for Barcodes and returns a list of (exam_id,
    pagenr, page_io) - for every page one entry. Unmatched pages are stored as
    `("parse_failure", "", page_io)` in that list.

    If passed, `callback()` is called after each scanned page
    """

    returnval: List[ScannedPage]

    def __init__(
        self,
        pdf_pages: Sequence[PdfProvider],
        dpi=250,
        callback=None,
        rescan: bool = False,
        code_placement: CodePlacement = CodePlacement.BOTTOM,
    ):
        threading.Thread.__init__(self)
        self.pdf_pages = pdf_pages
        self.dpi = dpi
        self.callback = callback
        self.rescan = rescan
        self.code_placement = code_placement

        self.executor = concurrent.futures.ThreadPoolExecutor()
        self.returnval = []

    def cancel(self):
        self.executor.shutdown(wait=False, cancel_futures=True)

    def run(self):
        try:
            self.returnval = list(self.executor.map(self.process_page, self.pdf_pages))
        except (concurrent.futures.CancelledError, RuntimeError):
            return None

    def process_page(self, page: PdfProvider) -> ScannedPage:
        result = process_pdf_page(page, self.dpi, self.code_placement)
        if result.exam_id is None and self.rescan:
            result = process_pdf_page(page, int(self.dpi * 1.5), self.code_placement)
        if self.callback is not None:
            self.callback(result.exam_id is not None)
        return result
