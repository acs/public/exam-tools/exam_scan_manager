from collections import namedtuple
from pathlib import Path
from typing import Optional, Callable, Any

from gi.repository import Gtk

from .exam import ScannedPage
from .examid_entry import ExamIdEntry
from .gui_helpers import get_content
from .pdf_view import PdfView

NewData = namedtuple("NewData", ["exam_id", "page_nr", "deleted"])


@Gtk.Template(
    filename=str((Path(__file__) / "../glade/Exam_Parameter_Entries.glade").resolve())
)
class ExamParametersBox(Gtk.Box):
    __gtype_name__ = "entries_box"

    delete_button = Gtk.Template.Child("delete_button")
    exam_id_box = Gtk.Template.Child("exam_id_box")
    page_nr_entry = Gtk.Template.Child("page_nr_entry")
    apply_button = Gtk.Template.Child("apply_button")

    def __init__(
        self,
        exam_ids: Optional[Gtk.ListStore] = None,
    ):

        super(Gtk.Box, self).__init__()

        self.delete_button.set_active(False)
        self.delete_button.connect("clicked", self.delete_clicked)

        self.exam_id_entry = ExamIdEntry(exam_ids)
        self.exam_id_box.pack_end(self.exam_id_entry, False, True, 0)
        self.exam_id_entry.connect("activate", self.focus_page_nr_entry)
        self.exam_id_entry.connect("changed", self.update_widgets)

        self.page_nr_entry.connect("changed", self.update_widgets)
        self.page_nr_entry.connect("activate", self.focus_apply_button)

        self.apply_button.connect("clicked", self.apply_clicked)

        # To determine whether something has changed
        self.initial_state: Optional[NewData] = None

        self.set_sensitive(False)
        self.callback: Optional[Callable[[object, Optional[NewData], Any], None]] = None
        self.callback_args: Optional[Any] = None

    def activate(
        self,
        exam_id: Optional[str],
        page_nr: Optional[int],
        deleted: bool,
        callback: Callable[[object, Optional[NewData], Any], None],
        *cb_args
    ):
        """
        callback function to be called when apply is pressed
        cb_args arguments for the callback upon apply press
        """
        self.callback = callback
        self.callback_args = cb_args

        self.set_sensitive(True)
        self.delete_button.set_active(deleted)
        self.exam_id_entry.set_sensitive(not deleted)
        self.page_nr_entry.set_sensitive(not deleted)

        self.initial_state = NewData(exam_id, page_nr, deleted)

        if exam_id is not None:
            self.exam_id_entry.set_text(str(exam_id))
        else:
            self.exam_id_entry.set_text("")

        if page_nr is not None:
            self.page_nr_entry.set_text(str(page_nr))
        else:
            self.page_nr_entry.set_text("")
        self.update_widgets(None)

    def update_widgets(self, widget):
        exam_id = get_content(self.exam_id_entry, str)
        page_nr = get_content(self.page_nr_entry, int)
        is_delete = self.delete_button.get_active()
        current_state = NewData(exam_id, page_nr, is_delete)
        has_changed = current_state != self.initial_state
        valid_input = is_delete or (exam_id is not None and page_nr is not None)
        self.apply_button.set_sensitive(has_changed and valid_input)

    def focus_page_nr_entry(self, widget):
        self.page_nr_entry.grab_focus()

    def focus_apply_button(self, widget):
        self.apply_button.grab_focus()

    def delete_clicked(self, widget):
        sensitive = not self.delete_button.get_active()
        self.exam_id_entry.set_sensitive(sensitive)
        self.page_nr_entry.set_sensitive(sensitive)
        self.update_widgets(widget)

    def apply_clicked(self, widget):
        new_data = NewData(
            self.exam_id_entry.get_text(),
            int(self.page_nr_entry.get_text()),
            self.delete_button.get_active(),
        )
        self.callback(new_data, *self.callback_args)
