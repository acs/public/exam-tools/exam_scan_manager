import os
import subprocess
import sys
from pathlib import Path

from gi.repository import Gdk, Gtk

provider = Gtk.CssProvider()
provider.load_from_data(
    """
    #failable_entry.red { background: LightCoral; }
    """.encode()
)


def get_content(entry, target_type):
    """
    Helper fn that queries an gtk entry and colors the entry box upon invalid content
    Returns the content cast into `desired_type`
    """
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

    text = entry.get_text()
    entry.set_name("failable_entry")
    if text == "":
        entry.get_style_context().remove_class("red")
        return None
    try:
        ret = target_type(text)
        entry.get_style_context().remove_class("red")
        return ret
    except ValueError:
        # print("Invalid entry: " + text)
        entry.get_style_context().add_class("red")
        return None


def get_content_list(entry, target_type):
    """
    Helper fn that queries an gtk entry and colors the entry box upon invalid content
    Returns the content as a list splitted at ',' and cast into `desired_type`
    """
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

    text = entry.get_text()
    entry.set_name("failable_entry")
    if text == "":
        entry.get_style_context().remove_class("red")
        return None
    try:
        content_list = list(map(target_type, text.strip(",").split(",")))
        entry.get_style_context().remove_class("red")
        return content_list
    except ValueError:
        entry.get_style_context().add_class("red")
        return None


def show_about_dialog(parent):
    gladefile = Path(__file__) / "../glade/exam_manager_about.glade"
    builder = Gtk.Builder()
    builder.add_from_file(str(gladefile.resolve()))
    dialog = builder.get_object("exam_scan_about")
    dialog.set_transient_for(parent)
    dialog.run()
    dialog.destroy()


def open_file(filename):
    """
    opens a file with the default handler.
    Kudos to https://stackoverflow.com/a/17317468/6551168
    """
    if sys.platform == "win32":
        os.startfile(filename)
    else:
        opener = "open" if sys.platform == "darwin" else "xdg-open"
        subprocess.call([opener, filename])


def successful_with_open_folder_dialog(parent_window, message: str, path: Path):
    dialog = Gtk.MessageDialog(
        parent_window,
        Gtk.DialogFlags.MODAL
        | Gtk.DialogFlags.DESTROY_WITH_PARENT
        | Gtk.DialogFlags.USE_HEADER_BAR,
        Gtk.MessageType.INFO,
        Gtk.ButtonsType.OK,
        message,
    )
    dialog.add_button("Open Folder", 42)
    dialog.show_all()
    response = dialog.run()
    if response == 42:
        open_file(path)
    dialog.destroy()
