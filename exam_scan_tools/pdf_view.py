from pathlib import Path

from gi.repository import Gdk, Gtk

from .pdf import PdfProvider, default_pdf


@Gtk.Template(filename=str((Path(__file__) / "../glade/Pdf_View.glade").resolve()))
class PdfView(Gtk.Overlay):
    __gtype_name__ = "pdf_view"

    scrolled_window = Gtk.Template.Child("scrolled_window")
    vp = Gtk.Template.Child("viewport")
    darea = Gtk.Template.Child("darea")
    zoom_button = Gtk.Template.Child("zoom_button")

    def __init__(self, pdf: PdfProvider = default_pdf):
        Gtk.Overlay.__init__(self)

        self.page = pdf.get_page()

        (pdf_x, pdf_y) = self.page.get_size()

        self.scrolled_window.set_max_content_width(pdf_x)

        self.darea.set_size_request(pdf_x, pdf_y)

        self.vp.set_size_request(pdf_x, pdf_y)
        self.scrolled_window.set_events(
            Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON_RELEASE_MASK
        )
        self.zoom_button.set_visible(False)

        self.scale = 1
        self.scrolled = False
        self.prev_pos = None
        self.drag_handle = None
        self.set_can_focus(False)

    def set_pdf(self, pdf: PdfProvider = default_pdf):
        self.page = pdf.get_page()
        self.darea.queue_draw()

    def middle_mouse_move(self, widget, event):
        """Move the view of the pfd with the mouse"""
        dist_x = self.prev_pos[0] - event.x
        dist_y = self.prev_pos[1] - event.y
        self.hadj.set_value(self.hadj.get_value() + dist_x)
        self.vadj.set_value(self.vadj.get_value() + dist_y)
        self.prev_pos = (event.x, event.y)

    @Gtk.Template.Callback()
    def middle_mouse_release(self, widget, event):
        """End mouse drags"""
        if self.drag_handle is not None:
            self.disconnect(self.drag_handle)
        self.drag_handle = None

    @Gtk.Template.Callback()
    def middle_mouse_pressed(self, widget, event):
        """Begin mouse drags"""
        self.vadj = self.scrolled_window.get_vadjustment()
        self.hadj = self.scrolled_window.get_hadjustment()
        self.prev_pos = (event.x, event.y)
        if self.drag_handle is not None:
            self.disconnect(self.drag_handle)
        self.drag_handle = self.connect("motion_notify_event", self.middle_mouse_move)

    @Gtk.Template.Callback()
    def on_scroll(self, widget, event):
        """Handles zoom in / zoom out on Ctrl+mouse wheel"""

        (pdf_x, pdf_y) = self.page.get_size()
        accel_mask = Gtk.accelerator_get_default_mod_mask()
        if event.state & accel_mask == Gdk.ModifierType.CONTROL_MASK:
            direction = event.get_scroll_deltas()[2]

            # TODO: This is still flaky and can be improved
            v_position = self.scrolled_window.get_vadjustment()
            h_position = self.scrolled_window.get_hadjustment()
            current_vpos = v_position.get_value()
            current_vmax = v_position.get_upper()
            current_hpos = h_position.get_value()
            current_hmax = h_position.get_upper()

            (mouse_x, mouse_y) = event.get_coords()
            mouse_habs = mouse_x + current_hpos
            mouse_vabs = mouse_y + current_vpos

            scroll_hrel = mouse_habs / current_hmax
            scroll_vrel = mouse_vabs / current_vmax

            if direction > 0 and self.scale > 0.3:  # scrolling down -> zoom out
                self.scale -= 0.1
            else:
                self.scale += 0.1

            self.scale = round(self.scale, 1)

            new_vmax = pdf_y * self.scale
            new_vscrol = new_vmax * scroll_vrel
            new_vpos = new_vscrol - mouse_y
            new_hmax = pdf_x * self.scale
            new_hscrol = new_hmax * scroll_hrel
            new_hpos = new_hscrol - mouse_x

            v_position.set_value(new_vpos)
            h_position.set_value(new_hpos)

            self.update_zoom()
            self.update_pdf()
            return True

    def update_zoom(self):
        natural_scale = self.get_natural_scale()
        relative_zoom = round(100 * self.scale / natural_scale)
        self.zoom_button.set_label(f"Zoom: {relative_zoom}%")
        if self.scale == natural_scale:
            self.zoom_button.set_visible(False)
            self.scrolled = False
        else:
            self.zoom_button.set_visible(True)
            self.scrolled = True

    @Gtk.Template.Callback()
    def draw_pdf(self, widget, context):
        # set background to gray
        context.set_source_rgb(0.9, 0.9, 0.9)
        context.paint()

        (frame_height, frame_width) = (
            self.vp.get_allocated_height(),
            self.vp.get_allocated_width(),
        )
        (pdf_x, pdf_y) = self.page.get_size()
        pdf_x *= self.scale
        pdf_y *= self.scale
        self.darea.set_size_request(pdf_x, pdf_y)

        translate_y = 0
        if frame_height > pdf_y:
            translate_y = frame_height / 2 - pdf_y / 2
        translate_x = 0
        if frame_width > pdf_x:
            translate_x = frame_width / 2 - pdf_x / 2
        context.translate(translate_x, translate_y)

        # draw white page background
        context.set_source_rgb(1, 1, 1)
        context.rectangle(0, 0, pdf_x, pdf_y)
        context.fill()

        # render pdf
        context.scale(self.scale, self.scale)
        if self.page is not None:
            self.page.render(context)

    def get_natural_scale(self) -> float:
        """This calculates the scaling factor so the page barely fits the viewport"""
        (pdf_x, pdf_y) = self.page.get_size()
        (frame_height, frame_width) = (
            self.vp.get_allocated_height(),
            self.vp.get_allocated_width(),
        )
        scale_x = frame_width / pdf_x
        scale_y = frame_height / pdf_y
        scale: float = round(min(scale_x, scale_y) - 0.05, 1)
        return scale

    @Gtk.Template.Callback()
    def on_resize(self, widget, data):
        pre_update_zoomed = self.scrolled
        self.update_zoom()
        if pre_update_zoomed:
            return
        self.scale = self.get_natural_scale()

    def update_pdf(self):
        (pdf_x, pdf_y) = self.page.get_size()
        pdf_x *= self.scale
        pdf_y *= self.scale
        self.darea.set_size_request(pdf_x, pdf_y)
        self.darea.queue_draw()

    @Gtk.Template.Callback()
    def on_zoom_clicked(self, _widget):
        self.scale = self.get_natural_scale()
        self.update_pdf()
