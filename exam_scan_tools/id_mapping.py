from gi.repository import Gtk
from pathlib import Path
import csv
from typing import Optional


class CsvMappingDialog:
    csv = None
    student_id_column: Optional[int] = None
    exam_id_column: Optional[int] = None

    def __init__(self, parent):
        gladefile = Path(__file__) / "../glade/CSV_Mapping_Dialog.glade"

        builder = Gtk.Builder()
        builder.add_from_file(str(gladefile.resolve()))

        self.dialog = builder.get_object("csv_mapping_dialog")
        self.dialog.set_transient_for(parent)

        self.apply_button = builder.get_object("apply_button")
        self.apply_button.set_sensitive(False)
        self.apply_button.connect("clicked", self.import_clicked)

        self.cancel_button = builder.get_object("cancel_button")
        self.cancel_button.connect("clicked", self.cancel_clicked)

        self.csv_chooser = builder.get_object("csv_chooser")
        self.csv_chooser.connect("file-set", self.file_set)

        self.exam_id_stack = builder.get_object("exam_id_stack")
        self.exam_id_column_combo = builder.get_object("exam_id_column_combo")
        self.exam_id_column_spin = builder.get_object("exam_id_column_spin")

        self.student_id_stack = builder.get_object("student_id_stack")
        self.student_id_column_combo = builder.get_object("student_id_column_combo")
        self.student_id_column_spin = builder.get_object("student_id_column_spin")

        self.header_checkbox = builder.get_object("header_checkbox")
        self.header_checkbox.connect("toggled", self.header_checkbox_toggled)

        self.contains_header = False

    def file_set(self, widget):
        self.apply_button.set_sensitive(True)
        self.header_checkbox.set_sensitive(True)
        self.student_id_stack.set_sensitive(True)
        self.exam_id_stack.set_sensitive(True)
        self.apply_button.get_style_context().add_class("suggested-action")

        self.csv = Path(self.csv_chooser.get_filename())
        with self.csv.open(newline="") as csv_f:
            if csv.Sniffer().has_header(csv_f.read(1024)):
                self.header_checkbox.set_active(True)
            else:
                self.analyze_columns()

    def header_checkbox_toggled(self, widget):
        self.contains_header = self.header_checkbox.get_active()
        if self.contains_header:
            self.student_id_stack.set_visible_child_name("with_header")
            self.exam_id_stack.set_visible_child_name("with_header")
            self.analyze_header()

        else:
            self.student_id_stack.set_visible_child_name("without_header")
            self.exam_id_stack.set_visible_child_name("without_header")
            self.analyze_columns()

    def analyze_columns(self):
        with self.csv.open(newline="") as csv_f:
            dialect = csv.Sniffer().sniff(csv_f.read(1024))
            csv_f.seek(0)
            reader = csv.reader(csv_f, dialect=dialect)
            col_nrs = len(list(filter(lambda name: name != "", reader.__next__())))
            self.exam_id_column_spin.get_adjustment().set_upper(col_nrs)
            self.student_id_column_spin.get_adjustment().set_upper(col_nrs)

    def analyze_header(self):
        with self.csv.open(newline="") as csv_f:
            dialect = csv.Sniffer().sniff(csv_f.read(1024))
            csv_f.seek(0)
            reader = csv.DictReader(csv_f, dialect=dialect)
            model = Gtk.ListStore(str)

            for name in filter(
                lambda name: name != "",
                map(lambda name: name.strip(), reader.fieldnames),
            ):
                model.append(
                    [
                        name,
                    ]
                )

            self.exam_id_column_combo.set_model(model)
            self.exam_id_column_combo.set_active(0)
            self.student_id_column_combo.set_model(model)
            self.student_id_column_combo.set_active(0)

    def cancel_clicked(self, widget):
        self.csv = None
        self.student_id_column = None
        self.exam_id_column = None
        self.dialog.destroy()

    def import_clicked(self, widget):
        # Import is only active after file selection
        assert self.csv is not None
        if self.contains_header:
            self.student_id_column = self.student_id_column_combo.get_active()
            self.exam_id_column = self.exam_id_column_combo.get_active()
        else:
            self.student_id_column = int(self.student_id_spin.get_value()) - 1
            self.exam_id_column = int(self.exam_id_spin.get_value()) - 1

        self.dialog.destroy()
