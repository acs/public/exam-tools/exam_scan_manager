import csv
import random
from concurrent import futures
from copy import deepcopy
from enum import Enum
from io import BytesIO
from logging import error, info
from pathlib import Path
from string import ascii_uppercase, digits
from threading import Thread
from typing import BinaryIO, Iterator, List, NamedTuple, Optional, TextIO, Tuple, cast

from barcode import Code128
from barcode.writer import ImageWriter
from PyPDF2 import PageObject, PdfFileReader, PdfFileWriter
from wand.drawing import Drawing
from wand.image import Image


class Position(Enum):
    TOP = 1
    BOTTOM = 2


class SplitMode(Enum):
    MULTIPLE = 1
    SINGLE = 2


class SeparationSettings(NamedTuple):
    mode: SplitMode
    page_range: List[int]
    filename: str


class BarcodePlacement(NamedTuple):
    pos: Position
    offset_lr: float
    offset_tb: float
    height: float


def page_size_cm(page: PageObject):
    """
    Reads the size of the pdf page returns `(width, height)` in cm
    """
    # The width of a page is given in pt
    return (
        float(page.mediaBox.getWidth()) * 25.4 / 720,
        float(page.mediaBox.getHeight()) * 25.4 / 720,
    )


def bar_position(
    placement: BarcodePlacement,
    page_width: float,
    page_height: float,
    bar_width: float,
    bar_height: float,
):
    """
    Calculates the position of the lower right side of the barcode depending on the positioning on
    the page. Returns `(pos_y, pos_x) All units in cm
    """
    return {
        Position.BOTTOM: (
            placement.offset_tb,
            (page_width - bar_width) / 2 + placement.offset_lr,
        ),
        Position.TOP: (
            page_height - placement.offset_tb - bar_height,
            (page_width - bar_width) / 2 + placement.offset_lr,
        ),
    }[placement.pos]


def generate_barcode_page(
    bar_string: str, placement: BarcodePlacement, page_width: float, page_height: float
):
    """
    Generates a Barcodet image which can be overlayed to the bottom left of a
    pdf page.
    """

    # bar_code = pybarcode.create(bar_string)

    bar_bytes = BytesIO()
    # bar_code.png(bar_bytes)
    writer = ImageWriter()
    code = Code128(str(bar_string), writer=writer)
    rendered = code.render(
        {
            "text_distance": 1.0,
            "module_width": 0.6,
            "module_height": 7.0,
            "font_size": 20,
            "write_text": False,
        }
    )
    writer.write(rendered, bar_bytes)
    bar_bytes.seek(0)

    ppcm = 100  # Pixels per cm

    with Image(file=bar_bytes) as bar_img:
        (width, height) = bar_img.size
        ratio = width / height
        bar_img.resize(
            width=int(ppcm * placement.height * ratio),
            height=int(ppcm * placement.height),
            blur=0.3,
        )
        (scaled_width, scaled_height) = bar_img.size

        # Also add ID as Text
        text_image = Image(
            width=int(scaled_width / 3), height=int(scaled_height), format="png"
        )
        with Drawing() as draw:
            draw.font_size = int(text_image.size[1] / 2)
            draw.text_alignment = "center"
            draw.text(
                int(text_image.size[0] / 2), int(text_image.size[1] * 3 / 4), bar_string
            )
            draw.draw(text_image)

        text_bar_img = Image(
            width=int(text_image.size[0] + scaled_width),
            height=int(scaled_height),
            format="png",
        )
        text_bar_img.composite(bar_img, 0, 0)
        text_bar_img.composite(text_image, scaled_width, 0)
        text_bar_img.resolution = (ppcm, ppcm)

        (combined_width, combined_height) = text_bar_img.size
        (pos_y_cm, pos_x_cm) = bar_position(
            placement,
            page_width,
            page_height,
            combined_width / ppcm,
            combined_height / ppcm,
        )

        blank_img = Image(
            width=int(pos_x_cm * ppcm + text_bar_img.size[0]),
            height=int(pos_y_cm * ppcm + scaled_height),
            format="pdf",
            units="pixelspercentimeter",
        )

        blank_img.resolution = (ppcm, ppcm)
        blank_img.composite(text_bar_img, int(pos_x_cm * ppcm), 0)

        return blank_img.convert("pdf")


def barcode_on_page(page: PageObject, exam_str: str, placement: BarcodePlacement):
    (page_width, page_height) = page_size_cm(page)
    new_page_io = BytesIO()
    generate_barcode_page(exam_str, placement, page_width, page_height).save(
        file=new_page_io
    )
    new_page_io.seek(0)
    barcode_filereader = PdfFileReader(new_page_io)
    page.mergePage(barcode_filereader.getPage(0))


def barcode_on_exam(
    exam_file: BinaryIO,
    output_stream: BinaryIO,
    exam_id: str,
    placement: BarcodePlacement,
    separation_tuple: Tuple[SeparationSettings, PdfFileWriter] = None,
):
    exam_pdf = PdfFileReader(exam_file)
    if exam_pdf.isEncrypted:
        exam_pdf.decrypt("")
    barcoded_exam = PdfFileWriter()

    for i, page in enumerate(exam_pdf.pages):
        info(f"generating barcode on exam {exam_id} - p.{i}")

        barcode_on_page(page=page, exam_str=f"{exam_id}-{i+1}", placement=placement)

        if separation_tuple is not None and i + 1 in separation_tuple[0].page_range:
            separation_tuple[1].addPage(page)
        else:
            barcoded_exam.addPage(page)

    barcoded_exam.write(cast(BytesIO, output_stream))


def barcode_on_single_page(
    exam_file: BinaryIO,
    page_nr: int,
    output_stream: BinaryIO,
    placement: BarcodePlacement,
):
    reader = PdfFileReader(exam_file)
    if reader.isEncrypted:
        reader.decrypt("")
    page = reader.getPage(page_nr)
    barcode_on_page(page=page, exam_str=f"1A2B3C4-{page_nr+1}", placement=placement)
    first_page_pdf = PdfFileWriter()
    first_page_pdf.addPage(page)
    first_page_pdf.write(cast(BytesIO, output_stream))


def nr_pdf_pages(exam_file: BinaryIO) -> int:
    exam_file.seek(0)
    reader = PdfFileReader(exam_file)
    if reader.isEncrypted:
        reader.decrypt("")
    nr_pages: int = reader.getNumPages()
    return nr_pages


class BarcodeGeneratorThread(Thread):
    """
    exam_file must be a BytesIO object
    """

    def __init__(
        self,
        exam_file_path: Path,
        output_folder: Path,
        exam_ids: Iterator[str],
        placement: BarcodePlacement,
        alternating: bool = False,
        separation: SeparationSettings = None,
        callback=None,
    ):
        Thread.__init__(self)

        self.executor = futures.ThreadPoolExecutor()
        with exam_file_path.open(mode="rb") as exam_file:
            self.exam_file = BytesIO(exam_file.read())
        self.exam_ids = exam_ids
        self.output_folder = output_folder
        self.placement = placement
        self.alternating = alternating

        self.separation = separation
        self.callback = callback

    def cancel(self):
        self.executor.shutdown(wait=False, cancel_futures=True)

    def run(self):
        try:
            results = list(self.executor.map(self.create_exam, self.exam_ids))

            if self.separation is not None and self.separation.mode == SplitMode.SINGLE:
                output_pdf = PdfFileWriter()
                for res in results:
                    for i in range(res.getNumPages()):
                        output_pdf.addPage(res.getPage(i))
                with Path(self.separation.filename).open("wb") as outfile:
                    output_pdf.write(outfile)

        except futures.CancelledError:
            error("Barcode generation was interrupted")
            return None

    def create_exam(self, exam_id) -> Optional[PdfFileWriter]:
        """
        Puts barcodes on the exam pages and either writes them into files in the output folder or
        returns the file data for merging
        """
        # parallel file access is a bad idea. Use copies of that file
        file_copy = deepcopy(self.exam_file)
        file_copy.seek(0)
        with (self.output_folder / (exam_id + ".pdf")).open(mode="wb") as outfile:
            if self.separation is None:
                barcode_on_exam(file_copy, outfile, exam_id, self.placement)
            else:
                sep_tuple = (deepcopy(self.separation), PdfFileWriter())
                barcode_on_exam(file_copy, outfile, exam_id, self.placement, sep_tuple)
                if self.separation.mode == SplitMode.MULTIPLE:
                    with Path(self.separation.filename + f"{exam_id}.pdf").open(
                        "wb"
                    ) as outfile:
                        sep_tuple[1].write(outfile)
                else:
                    if self.callback is not None:
                        self.callback()
                    return sep_tuple[1]

        if self.callback is not None:
            self.callback()
        return None


def generate_ids(count: int, id_length: int = 6) -> List[str]:
    ids: List[str] = []
    while len(ids) < count:
        new_id = "".join(random.choices(ascii_uppercase + digits, k=id_length))
        if new_id not in ids:
            ids.append(new_id)
    return ids


def write_list_to_file(ids: List[str], file: TextIO):
    for exam_id in ids:
        csv.writer(file).writerow([exam_id])
